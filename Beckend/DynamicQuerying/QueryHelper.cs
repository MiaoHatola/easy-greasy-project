﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.DynamicQuerying.Queries;

namespace Beckend.DynamicQuerying
{
    public enum QueryType { InsertQuery, SelectQuery , EditQuery, DeleteQuery }

    public class QueryHelper
    {
        public static QueryType GetEnumFromString(string str)
        {
            return (QueryType)Enum.Parse(typeof(QueryType), str);
        }
        public static QueryType GetEnumFromInt(int n)
        {
            return (QueryType)n;
        }
        public static Type GetTypeFromInt(int n)
        {
            QueryType qt = GetEnumFromInt(n);
            switch (qt)
            {
                case QueryType.SelectQuery: return typeof(SelectQuery);
                case QueryType.InsertQuery: return typeof(InsertQuery);
                case QueryType.EditQuery: return typeof(EditQuery);
                case QueryType.DeleteQuery: return typeof(DeleteQuery);
                default:
                    return null;
            }
        }
        public static string[] GetNames()
        {
            string[] ans = new string[Enum.GetValues(typeof(QueryType)).Length];
            for (int i = 0; i < ans.Length; i++)
            {
                ans[i] = GetEnumFromInt(i).ToString();
            }
            return ans;
        }
        public static string GetQueryTypesToString()
        {
            string ans = "";
            string o = "Operation";
            string[] tmp;
            for (int i = 0; i < Enum.GetNames(typeof(QueryType)).Length; i++)
            {
                tmp = GetEnumFromInt(i).ToString().Split('.');
                ans += o + " " + (i + 1) + "." + tmp[tmp.Length - 1] + "\n";
            }
            return ans;
        }
        private static MethodInfo[] pack(MethodInfo[] arr, int count)
        {
            MethodInfo[] ans = new MethodInfo[count];
            int j = 0;
            try
            {
                for (int i = 0; i < arr.Length; i++)
                    if (arr[i] != null)
                    {
                        ans[j] = arr[i];
                        j++;
                    }
            }
            catch
            {
                throw new ArgumentException("wrong count argument (QueryHelper.pack)");
            }
            return ans;
        }
    }
}