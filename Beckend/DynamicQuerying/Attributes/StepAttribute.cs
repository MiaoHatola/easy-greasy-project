﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beckend.DynamicQuerying.Attributes
{
    public class StepAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="min">the minimum number of time this step should be called (0 = unmendatory)</param>
        /// <param name="max">the maximum number of time this step should be called (-1 = infinity)</param>
        public enum StepOptionType { MultiOptional, SingleOption, FreeText }
        public StepAttribute(string name, StepOptionType stepType, int min, int max)
        {
            if (min < 1)
                throw new ArgumentException("Min can't be less than 1. Please use the optional constructor.");
            this.Min = min;
            this.Max = (max != -1) ? max : int.MaxValue;
            IsOptional = false;
            this.StopSign = this.OptionQuestion = null;
            this.StepType = stepType;
        }
        public StepAttribute(string name, StepOptionType stepType, int min, int max, string stopSign, string optionalQuestion)
        {
            this.Min = min;
            this.Max = (max != -1) ? max : int.MaxValue;
            this.StopSign = stopSign;
            this.OptionQuestion = optionalQuestion;
            IsOptional = true;
            this.StepType = stepType;
        }
        public StepOptionType StepType { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public string StopSign { get; set; }
        public string OptionQuestion { get; set; }
        public bool IsOptional { get; set; }
    }
}