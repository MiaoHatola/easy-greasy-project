﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beckend.DynamicQuerying.Exceptions
{
    public class IdiotProgrammerException : Exception
    {
        public IdiotProgrammerException(string msg) 
            : base(msg)
        { }
    }
}
