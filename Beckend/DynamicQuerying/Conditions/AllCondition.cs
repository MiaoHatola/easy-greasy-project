﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Beckend.Entity;

namespace Beckend.DynamicQuerying.Conditions
{
    [Serializable]
    public class AllCondition : ICondition
    {
        public bool Check(DynamicEntity obj)
        {
            return this.Check(obj, false);
        }
        public bool Check(DynamicEntity obj, bool includeDeleted)
        {
            if (obj.IsDeleted() && !includeDeleted)
                return false;
            return true;
        }
        public string ToSQL()
        {
            return "ALL";
        }
        public override string ToString()
        {
            return "All";
        }
    }
}
