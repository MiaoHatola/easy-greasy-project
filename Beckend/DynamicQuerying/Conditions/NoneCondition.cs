﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Beckend.Entity;

namespace Beckend.DynamicQuerying.Conditions
{
    [Serializable]
    public class NoneCondition : ICondition
    {
        public bool Check(DynamicEntity obj)
        {
            return Check(obj, false);
        }
        public bool Check(DynamicEntity obj, bool includeDeleted)
        {
            return false;
        }
        public string ToSQL()
        {
            return "NONE";
        }
        public override string ToString()
        {
            return "None";
        }
    }
}
