﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.Entity;

namespace Beckend.DynamicQuerying.Conditions
{
    [Serializable]
    public class OrCondition : ICondition
    {
        private AndCondition[] conds;

        public OrCondition(string expression, PropertyInfo[] allProperties)
        {
            string[] tmp = expression.Split('|');
            conds = new AndCondition[tmp.Length];

            for (int i = 0; i < tmp.Length; i++)
                conds[i] = new AndCondition(tmp[i], allProperties);
        }
        bool ICondition.Check(DynamicEntity obj)
        {
            return this.Check(obj, false);
        }
        public bool Check(DynamicEntity obj, bool includeDeleted)
        {
            for (int i = 0; i < conds.Length; i++)
                if (conds[i].Check(obj, includeDeleted))
                    return true;
            return false;
        }
        public string ToSQL()
        {
            string ans = "";
            if (conds.Length > 0)
                ans += conds[0].ToSQL();
            for (int i = 1; i < conds.Length; i++)
                ans += " OR " + conds[i].ToSQL();
            return ans;
        }
        public override string ToString()
        {
            string ans = "";
            if (conds.Length > 0)
                ans = conds[0].ToString();
            for (int i = 1; i < conds.Length; i++)
                ans += " | (" + conds[i].ToString() + ")";

            return ans;
        }
    }
}
