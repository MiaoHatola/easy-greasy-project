﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.Entity;

namespace Beckend.DynamicQuerying.Conditions
{
    [Serializable]
    public class AndCondition : ICondition
    {
        SingleCondition[] conds;

        public AndCondition(PropertyInfo[] pi, int[] comp, string[] args)
        {
            conds = new SingleCondition[pi.Length];

            for (int i = 0; i < conds.Length; i++)
                conds[i] = new SingleCondition(pi[i], comp[i], args[i]);
        }
        public AndCondition(string expression, PropertyInfo[] allProperties)
        {
            string[] tmp = expression.Split('&');

            conds = new SingleCondition[tmp.Length];
            for (int i = 0; i < tmp.Length; i++)
                conds[i] = new SingleCondition(tmp[i], allProperties);
        }
        bool ICondition.Check(DynamicEntity obj)
        {
            return this.Check(obj, false);
        }
        public bool Check(DynamicEntity obj, bool includeDeleted)
        {
            for (int i = 0; i < conds.Length; i++)
                if (!conds[i].Check(obj, includeDeleted))
                    return false;
            return true;
        }
        public string ToSQL()
        {
            string ans = "";
            if (conds.Length > 0)
                ans += conds[0].ToSQL();
            for (int i = 1; i < conds.Length; i++)
                ans += " AND " + conds[i].ToSQL();
            return ans;
        }
        public override string ToString()
        {
            string ans = "";
            if (conds.Length > 0)
            {
                ans = conds[0].ToString();
                for (int i = 1; i < conds.Length; i++)
                    ans += " & " + conds[i].ToString();
            }
            return ans;
        }
    }
}
