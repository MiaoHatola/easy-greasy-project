﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beckend.DynamicQuerying.Conditions
{
    class ConditionHelper
    {
        public static ICondition All = new AllCondition();
        public static ICondition None = new NoneCondition();
    }
}