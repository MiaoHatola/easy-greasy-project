﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Beckend.Entity;

namespace Beckend.DynamicQuerying.Conditions
{
    public interface ICondition
    {
        bool Check(DynamicEntity obj);
        string ToSQL();
    }
}
