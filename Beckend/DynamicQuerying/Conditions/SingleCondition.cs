﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.Entity;

namespace Beckend.DynamicQuerying.Conditions
{
    [Serializable]
    public class SingleCondition : ICondition
    {
        private PropertyInfo pi;
        private int comp;
        private string arg;

        public PropertyInfo Property { get { return this.pi; } }
        public int Comparer { get { return this.comp; } }
        public string Argument { get { return this.arg; } }

        public SingleCondition(PropertyInfo pi, int comp, string arg)
        {
            this.pi = pi;
            this.comp = comp;
            this.arg = arg;
        }
        public SingleCondition(string expression, params PropertyInfo[] allProperties)
        {
            string[] tmp = cutEdges(expression).Split(' ');
            this.pi = findProperty(allProperties, tmp[0]);
            if (pi == null)
                this.pi = allProperties[int.Parse(tmp[0])];
            this.comp = DecideComparer(tmp[1]);
            this.arg = tmp[2];
        }
        private PropertyInfo findProperty(PropertyInfo[] pi, string name)
        {
            name = name.ToUpper();
            for (int i = 0; i < pi.Length; i++)
                if (pi[i].Name.ToUpper() == name)
                    return pi[i];
            return null;
        }
        private string cutEdges(string str)
        {
            if (str.Length == 0)
                return str;
            int i = 0;
            while (str[i] == ' ')
                i++;
            int j = str.Length;
            while (str[j - 1] == ' ')
                j--;
            return str.Substring(i, j - i);
        }
        private int DecideComparer(string input)
        {
            switch (input.ToUpper())
            {
                case ">": case "GREATER": case "GREATERTHAN": return 1;
                case "<": case "SMALLER": case "SMALLERTHAN": return -1;
                case "=": case "HAS": case "IS": case "EQUALS": return 0;
                default: throw new ArgumentException("invalid comparer (Condition.DecideComparer)");
            }
        }
        private string toComparer(int n)
        {
            switch (n)
            {
                case 1: return ">";
                case -1: return "<";
                default: return "=";
            }
        }

        bool ICondition.Check(DynamicEntity obj)
        {
            return this.Check(obj, false);
        }
        public bool Check(DynamicEntity obj, bool includeDeleted)
        {

            if (obj.IsDeleted() && !includeDeleted)
                return false;

            IComparable value = (IComparable)(pi.GetValue(obj));

            if (pi.GetValue(obj) is ICollection<DynamicEntity>)
            {
                ICollection<DynamicEntity> col = value as ICollection<DynamicEntity>;

                if (comp == 0)
                    return col.Contains(Convert.ChangeType(arg, pi.PropertyType));
                else
                    return col.Any(item => ((IComparable)item).CompareTo((IComparable)arg) == comp);
            }
            if (pi.GetValue(obj) is ICollection<int>)
            {
                ICollection<int> col = pi.GetValue(obj) as ICollection<int>;

                if (comp == 0)
                    return col.Contains(int.Parse(arg));
                else
                    return col.Any(item => ((IComparable)item).CompareTo(int.Parse(arg)) == comp);
            }
            if (pi.GetValue(obj) is ICollection<string>)
            {
                ICollection<string> col = pi.GetValue(obj) as ICollection<string>;

                if (comp == 0)
                    return col.Contains(Convert.ChangeType(arg, pi.PropertyType));
                else
                    return col.Any(item => ((IComparable)item).CompareTo((IComparable)arg) == comp);
            }
            else if (value is Enum)
            {
                try
                {
                    // first chcracter of the enum type's name is always uppercase, and the rest is lowercase
                    arg = arg.ToUpper();

                    if (value.ToString().ToUpper().CompareTo(arg) == comp)
                        return true;
                    else
                        return false;
                }
                // currentArg is not of the current enum's type
                catch (ArgumentException)
                {
                    return false;
                }
            }
            else if (value is string)
            {
                // in case value is string, compare between the objects in a non case sensitive way
                if (value.ToString().ToUpper().CompareTo(arg.ToString().ToUpper()) == comp)
                    return true;
                else return false;
            }
            else
            {
                // case of primitives, datetime, string, decimal...
                try
                {
                    if (value.CompareTo(Convert.ChangeType(arg, pi.PropertyType)) == comp)
                        return true;
                    else return false;
                }
                catch
                {
                    return false;
                }
            }
        }
        public string ToSQL()
        {
            return pi.Name.ToUpper() + toComparer(comp) + "\'" + arg + "\'";
        }
        public override string ToString()
        {
            char c;
            if (comp == 0)
                c = '=';
            else if (comp > 0)
                c = '>';
            else
                c = '<';
            string ans = pi.Name + " " + c + " ";
            string s = "";
            if (pi.Name.ToUpper() == "PASSWORD")
                for (int i = 0; i < arg.Length; i++)
                    s += "*";
            else
                s = arg;
            return ans + s;
        }
    }
}
