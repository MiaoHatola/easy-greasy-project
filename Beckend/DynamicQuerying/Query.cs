﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.Entity;
using Beckend.DynamicQuerying.Queries;

namespace Beckend.DynamicQuerying
{
    [Serializable]
    public abstract class Query : DynamicEntity
    {
        protected Query[] sideEffects;
        protected List<DynamicEntity> querable;
        protected string dataName;
        protected object[] results;

        public string DataName
        {
            get { return this.dataName; }
        }
        public string QueryContent
        {
            get { return this.ToString(); }
        }

        public Query()
        {
            sideEffects = null;
            querable = null;
            results = null;
            dataName = "";
        }
        public override Type GetEntityType()
        {
            return typeof(Query);
        }
        public override string ToString()
        {
            return "Query";
        }
        public void SetDataName(string dataName)
        {
            this.dataName = dataName;
        }
        public void SetQuerable(List<DynamicEntity> querable)
        {
            this.querable = querable;
        }
        public abstract object[] RunQuery();
        
        public void SetResults(object[] results)
        {
            this.results = results;
        }
        public object[] GetResults()
        {
            return this.results;
        }
        public virtual Query[] GetSideEffects()
        {
            return this.sideEffects;
        }
        public abstract string GetSQLCommand();
        public abstract string ToSelectSQL(); 
    }
}
