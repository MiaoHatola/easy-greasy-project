﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.Entity;
using Beckend.Entity.BasicEntities;
using Beckend.DynamicQuerying.Conditions;

namespace Beckend.DynamicQuerying.Queries
{
    [Serializable]
    public class InsertQuery : Query
    {
        public static string InsertionFailed = "Failed to insert new data\n";
        DynamicEntity[] newData;

        public void SetInsert(DynamicEntity data)
        {
            SetInsert(new DynamicEntity[] { data });
        }
        public void SetInsert(DynamicEntity[] data)
        {
            this.newData = data;
        }
        public override object[] RunQuery()
        {
            List<object> ans = new List<object>();

            try
            {
                foreach (DynamicEntity item in newData)
                {
                    this.querable.Add(item);
                    ans.Add(item);
                }
            }
            catch
            {
                this.results = null;
            }
            SetResults(ans.ToArray());
            return GetResults();
        }
        public override Type GetEntityType()
        {
            return typeof(InsertQuery);
        }
        public override string GetSQLCommand()
        {
            Type T = newData[0].GetEntityType();
            return GetSingleSQLCommand(T, newData);
            /*
            while (T != typeof(DynamicEntity))
            {
                lst.Add(GetSingleSQLCommand(T, newData));
                T = T.BaseType;
            }
            */
        }
        private string GetSingleSQLCommand(Type T, object[] obj)
        {
            FieldInfo[] fi = EntityHelper.RemoveIDProperty(EntityHelper.GetFields(T, true));
            object[] arr = new object[fi.Length];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = fi[i].Name.ToUpper();
            
            string ans = "INSERT INTO [" + dataName + "] (" + ConvertArrToSql(arr, false) + ") VALUES ";

            // iterate over all the pending objects
            for (int i = 0; i < obj.Length; i++)
            {
                for (int k = 0; k < arr.Length; k++)
                {
                    arr[k] = fi[k].GetValue(obj[i]);
                    
                }
                ans += "(" + ConvertArrToSql(arr, true) + ") ";
                
                // in case there are some upcoming objects
                if (i + 1 < obj.Length)
                    ans += " , ";
            }
            return ans;
        }
        private string ConvertArrToSql(object[] input, bool shupshik)
        {
            string ans = "";
            string s = (shupshik) ? "'" : "";
            if (input.Length > 0)
            {
                // first field name
                if (input[0] is int)
                    ans += input[0];
                else
                    ans += s + input[0] + s;

                // the rest of the field names
                for (int i = 1; i < input.Length; i++)
                {
                    if (input[i] is int)
                        ans += ", "+input[i];
                    else
                        ans += ", " + s + input[i] + s;
                }
            }
            return ans;
        }
        public override string ToSelectSQL()
        {
            if (newData.Length <= 0)
                return "";
            string ans = "SELECT * FROM [" + dataName + "] WHERE ";
            FieldInfo[] fi = EntityHelper.RemoveIDProperty(EntityHelper.GetFields(this.newData[0].GetEntityType()));

            ans += getSqlSearchCondition(ans, fi, 0);
            for (int i = 1; i < newData.Length; i++)
            {
                ans += " OR (" + getSqlSearchCondition(ans, fi, i) + ")";
            }

            return ans;
        }

        private string getSqlSearchCondition(string ans, FieldInfo[] fi, int i)
        {
            ans = fi[0].Name + "='" + fi[0].GetValue(newData[i]) + "'";
            for (int k = 1; k < fi.Length; k++)
            {
                ans += " AND " + fi[k].Name + "='" + fi[k].GetValue(newData[i]) + "'";
            }
            return ans;
        }
        public override string ToString()
        {
            string ans = "Insert To " + dataName + " ";
            
            if (newData.Length > 0)
                ans += newData[0].ToString();
            for (int i = 1; i < newData.Length; i++)
                ans += " & " + newData[i].ToString();

            return ans;
        }
    }
}
