﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.Entity;
using Beckend.DynamicQuerying.Conditions;

namespace Beckend.DynamicQuerying.Queries
{
    [Serializable]
    public class SelectQuery : WhereQuery
    {
        public static string NoneFound = "nothing was selected\n";
        //PropertyInfo[] whatPropertys;
        
        /*
        public void SetSelect(PropertyInfo property)
        {
            SetSelect(new PropertyInfo[] { property });
        }
        public void SetSelect(PropertyInfo[] propertys)
        {
            this.whatPropertys = propertys;
        }
         */

        public override object[] RunQuery()
        {
            IEnumerable<DynamicEntity> @enum = RunLinq();
            List<object> ans = new List<object>();
            foreach (DynamicEntity item in @enum)
            {
                ans.Add(item);
            }
            SetResults(ans.ToArray());
            return this.results;
        }
        public override Type GetEntityType()
        {
            return typeof(SelectQuery);
        }
        public override string GetSQLCommand()
        {
            if (condition is NoneCondition)
                return "SELECT () FROM [" + dataName + "]";

            string ans = "SELECT * FROM [" + dataName + "]";
            
            if (!(condition is AllCondition))
                ans += " WHERE " + condition.ToSQL();
            
            return ans;
        }
        public override string ToString()
        {
            return "Select " + "From " + dataName + " Where " + condition.ToString();
        }
    }
}
