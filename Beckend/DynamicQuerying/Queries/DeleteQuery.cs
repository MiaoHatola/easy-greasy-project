﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.Entity;
using Beckend.DynamicQuerying.Conditions;

namespace Beckend.DynamicQuerying.Queries
{
    [Serializable]
    public class DeleteQuery : WhereQuery
    {
        
        public DeleteQuery()
        {
            sideEffects = new Query[0];
        }
        public override object[] RunQuery()
        {
            IEnumerable<DynamicEntity> @enum = RunLinq();
            Query[] tmp, combine;
            int count = 0;

            List<object> ans = new List<object>();

            foreach (DynamicEntity item in @enum.ToList())
            {
                // remember all the side effects of removing the item so we can apply them later
                tmp = item.GetDeleteActions();
                if (tmp != null)
                {
                    combine = new Query[sideEffects.Length + tmp.Length];
                    sideEffects.CopyTo(combine, 0);
                    tmp.CopyTo(combine, sideEffects.Length);
                    sideEffects = combine;
                    combine = tmp = null;
                }
                if (item.Delete())
                    this.querable.Remove(item);
                ans.Add(item);

                count++;
            }
            SetResults(ans.ToArray());
            return GetResults();
        }
        public override string GetSQLCommand()
        {
            string ans = "DELETE [" + dataName + "] FROM " + dataName;
            if (!(condition is AllCondition))
                ans += " WHERE " + condition.ToString();
            return ans;
        }
        public override string ToString()
        {
            return "Delete From " + dataName + " Where " + condition.ToString();
        }
        public override Type GetEntityType()
        {
            return typeof(DeleteQuery);
        }
    }
}
