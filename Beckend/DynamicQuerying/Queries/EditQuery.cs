﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.Entity;
using Beckend.DynamicQuerying.Conditions;

namespace Beckend.DynamicQuerying.Queries
{
    [Serializable]
    public class EditQuery : WhereQuery
    {
        PropertyInfo[] PropertysToSet;
        string[] args;
        
        public override Type GetEntityType()
        {
            return typeof(EditQuery);
        }
        public override string ToString()
        {
            string ans = "Edit [" + dataName + "] Where " + condition.ToString() + ": \n";
            for (int i = 0; i < PropertysToSet.Length; i++)
                ans += PropertysToSet[i].Name + "-->" + args[i] + "\n";

            return ans;
        }

        public string GetEditSQL(DynamicEntity obj, string dataname)
        {
            if (obj.ID < 0)
                throw new Beckend.DynamicQuerying.Exceptions.IdiotProgrammerException("You are trying to edit an object with ID = -1");

            FieldInfo[] fi = EntityHelper.RemoveIDProperty(EntityHelper.GetFields(obj.GetEntityType()));

            string SQL = "UPDATE [" + dataname + "] SET ";
            foreach (FieldInfo field in fi)
            {
                SQL += field.Name + "='" + field.GetValue(obj) + "', ";
            }
            SQL = SQL.Substring(0, SQL.Length - 2);
            SQL += " WHERE ID=" + obj.ID;
            return SQL;
        }
        public void SetEdit(PropertyInfo Property, string arg)
        {
            SetEdit(new PropertyInfo[] { Property }, new string[] { arg });
        }
        public void SetEdit(PropertyInfo[] Propertys , string[] args)
        {
            this.PropertysToSet = Propertys;
            this.args = args;
        }
        public override string GetSQLCommand()
        {
            string ans = "UPDATE " + dataName;
            ans += " SET ";
            if (args.Length > 0)
            {
                ans += PropertysToSet[0].Name.ToUpper() + "='" + args[0] + "'";
                for (int i = 1; i < args.Length; i++)
                    ans += ", " + PropertysToSet[i].Name.ToUpper() + "='" + args[i] + "'";
            }
            if (!(condition is AllCondition))
                ans += " WHERE " + condition.ToSQL();
            return ans;
        }
        public override object[] RunQuery()
        {
            IEnumerable<DynamicEntity> @enum = RunLinq();
            List<object> ans = new List<object>();
            foreach (DynamicEntity item in @enum)
            {
                for (int i = 0; i < PropertysToSet.Length; i++)
                {
                    PropertysToSet[i].SetValue(item, Convert.ChangeType(args[i], PropertysToSet[i].PropertyType));
                    if (!ans.Contains(item))
                        ans.Add(item);
                }
            }
            SetResults(ans.ToArray());
            return GetResults();
        }
    }
}
