﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.Entity;
using Beckend.Entity.BasicEntities;
using Beckend.DynamicQuerying.Conditions;
using Beckend.DynamicQuerying.Queries;

namespace Beckend.DynamicQuerying
{
    [Serializable]
    public abstract class WhereQuery : Query
    {
        protected ICondition condition;
        protected bool selectDeleted;

        protected IEnumerable<DynamicEntity> RunLinq()
        {
            IEnumerable<DynamicEntity> Query =
            from item in this.querable
            where Filter(item)
            select item;

            return Query;
        }
        protected bool Filter(DynamicEntity obj)
        {
            return this.condition.Check(obj);
        }
        
        public void SetWhere(ICondition cond, bool searchDeleted)
        {
            this.condition = cond;
            this.selectDeleted = searchDeleted;
        }
        public void SetWhere(ICondition cond)
        {
            SetWhere(cond, false);
        }
        public override string ToSelectSQL()
        {
            SelectQuery sq = new SelectQuery();
            sq.SetDataName(dataName);
            sq.SetWhere(this.condition);
            return sq.GetSQLCommand();
        }
    }
}