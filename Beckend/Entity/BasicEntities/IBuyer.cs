﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beckend.Entity.BasicEntities
{
    public interface IBuyer
    {
        string getCreditCardDetials();
        void setCreditCardDetials(string ccd);
    }
}
