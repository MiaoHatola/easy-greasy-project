﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beckend.Entity.BasicEntities
{
    [Serializable]
    public class BoughtProduct : DynamicEntity
    {
        private int productID;
        private string productName;
        private int productPrice;
        private int productQuantity;
        private int transactionID;

        public int ProductId
        {
            get
            {
                return productID;
            }
        }

        public string ProductName
        {
            get
            {
                return productName;
            }
        }

        public int ProductPrice
        {
            get
            {
                return productPrice;
            }
        }
        
        public int ProductQuantity
        {
            get
            {
                return productQuantity;
            }
            set
            {
                productQuantity += (value);
            }
        }
        public int TransactionID
        {
            get
            {
                return transactionID;
            }
            set
            {
                transactionID = value;
            }
        }

        public BoughtProduct(Product p,int quantity)
        {
            this.productID = p.ID;
            this.productName = p.ProductName;
            this.productPrice = p.Price;
            this.productQuantity = quantity;
        }
        public BoughtProduct(string productID, string productName, string productPrice, string productQuantity, string transactionID)
        {
            this.productID = int.Parse(productID);
            this.productName = productName;
            this.productPrice = int.Parse(productPrice);
            this.productQuantity = int.Parse(productQuantity);
            this.transactionID = int.Parse(transactionID);
        }

        public void addProducts(int quantity)
        {
            this.ProductQuantity = quantity;
        }

        public void reduceProducts(int quantity)
        {
            this.ProductQuantity = -quantity; 
        }

        public override Type GetEntityType()
        {
            return typeof(BoughtProduct);
        }
    }
}
