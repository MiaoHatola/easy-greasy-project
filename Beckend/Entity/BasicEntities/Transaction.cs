﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Beckend.DynamicQuerying.Attributes;
using Beckend.DynamicQuerying.Queries;
using Beckend.DynamicQuerying.Conditions;

namespace Beckend.Entity.BasicEntities
{
    public enum PaymentMethod { Cash, Check, CreditCard }

    [Serializable]
    public class Transaction : DynamicEntity
    {
        //private fields
        private string dateTime;
        private bool isAReturn;
        private PaymentMethod paymentMethod;
        private int customerID;
        private int totalPrice;

        //TODO init transaction reciept

        //public proparties
        public bool IsAReturn
        {
            get
            {
                return this.isAReturn;
            }
            set
            {
                this.isAReturn = value;
            }
        }
        public PaymentMethod PaymentMethod
        {
            get
            {
                return this.paymentMethod;
            }
        }
        public string EditablePaymentMethod
        {
            set
            {
                this.paymentMethod = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), value);
            }
        }
        public string DateTime
        {
            get
            {
                return this.dateTime;
            }
        }
        public int CustomerID
        {
            get
            {
                return customerID;
            }
            set
            {
                customerID = value;
            }
        }
        public int TotalPrice
        {
            get
            {
                return this.totalPrice;
            }
        }

        public Transaction(bool IsAReturn, PaymentMethod paymentMethod, int totalPrice, int customerID, DateTime datetime)
        {
            this.dateTime = datetime.ToString("MM/dd/yyyy HH:mm:ss.fff");
            this.isAReturn = IsAReturn;
            this.paymentMethod = paymentMethod;
            this.totalPrice = totalPrice;
            this.customerID = customerID;
        }
        public Transaction(string isAReturn, string paymentMethod,string totalPrice, string customerID, string datetime)
            : this( bool.Parse(isAReturn), (PaymentMethod)Enum.Parse(typeof(PaymentMethod), paymentMethod[0].ToString().ToUpper() + paymentMethod.Substring(1)), int.Parse(totalPrice), int.Parse(customerID), System.DateTime.Parse(datetime))
        { }

        public override Type GetEntityType()
        {
            return typeof(Transaction);
        }

    }
}