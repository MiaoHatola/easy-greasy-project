﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beckend.Entity.BasicEntities
{
    public enum ProductType { Vegetable, Fruit, Meat, Dairy, Spices, Beverage }
    [Serializable]
    public class Product : DynamicEntity , IComparable
    {
        //private fields
        private string productName;
        private ProductType productType;
        private int inventoryID;
        private int location;
        private int price;
        private int stockCount;
        private int amountOfSales;

        //public proparties
        public bool InStock
        {
            get
            {
                return this.stockCount > 0;
            }
        }
        public string ProductName
        {
            get
            {
                return this.productName;
            }
            set
            {
                this.productName = value;
            }
        }
        public int Location
        {
            get
            {
                return this.location;
            }
            set
            {
                this.location = value;
            }
        }
        public int InventoryID
        {
            get
            {
                return this.inventoryID;
            }
            set
            {
                this.inventoryID = value;
            }
        }
        public ProductType ProductType 
        {
            get
            {
                return this.productType;
            }
        }
        public string EditableProductType
        {
            set
            {
                this.productType = (ProductType)Enum.Parse(typeof(ProductType), value);
            }
        }
        public int StockCount
        {
            get
            {
                return this.stockCount;
            }
            set
            {
                if (value >= 0)
                {
                    this.stockCount = value;
                }
            }
        }
        public int Price
        {
            get
            {
                return this.price;
            }
            set
            {
                if (value < 0) throw new ArgumentException("stock count cant be under zero");
                this.price = value;
            }
        }
        public int AmountOfSales
        {
            get
            {
                return this.amountOfSales;
            }
            set
            {
                amountOfSales = value;
            }
        }

        public Product(string productName, int price, ProductType productType, int inventoryID, int location, int stockCount,int amountOfSales)
        {
            if (stockCount < 0 || price < 0) throw new ArgumentException("stock count cant be under zero");
            this.productName = productName;
            this.productType = productType;
            this.inventoryID = inventoryID;
            this.location = location;
            this.price = price;
            this.stockCount = stockCount;

            this.amountOfSales = amountOfSales;


        }
        public Product(string productName, string price, string productType, string inventoryID, string location, string stockCount, string amountOfSales)
            : this( productName, int.Parse(price), (ProductType)Enum.Parse(typeof(ProductType), productType), int.Parse(inventoryID), int.Parse(location), int.Parse(stockCount),int.Parse(amountOfSales)) { }
        public override Type GetEntityType()
        {
            return typeof(Product);
        }

        public int CompareTo(object obj)
        {
            Product p = obj as Product;
            return (this.amountOfSales < p.amountOfSales) ? 1 : (this.amountOfSales > p.amountOfSales) ? -1 : 0; 
        }
    }
}
