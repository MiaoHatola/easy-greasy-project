﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beckend.Entity.BasicEntities
{
    [Serializable]
    public class ClubMember : Person , IBuyer
    {
        //private fields
        private DateTime dateOfBirth;
        private string creditCardDetials;
        
        //public properties
        public string CreditCardDetials
        {
            get
            {
                return creditCardDetials;
            }
            set
            {
                creditCardDetials = value;
            }
        }
        public DateTime DateOfBirth
        {
            get
            {
                return this.dateOfBirth;
            }
            set
            {
                this.dateOfBirth = value;
            }
        }

        public ClubMember(string userName, string password, string type, string firstName, string lastName, int teudatZehut, DateTime dateOfBirth, Gender gender,string creditCardDetials)
            : base(userName, password, type,teudatZehut, firstName, lastName, gender)
        {
            this.creditCardDetials = creditCardDetials;
            this.dateOfBirth = dateOfBirth;
        }

        public ClubMember(string userName, string password, string type, string firstName, string lastName, string teudatZehut, string dateOfBirth, string gender,string creditCardDetials)
            : this(userName, password, type,firstName, lastName, int.Parse(teudatZehut), DateTime.Parse(dateOfBirth), (Gender)Enum.Parse(typeof(Gender), gender),creditCardDetials) { }
       
        public override Type GetEntityType()
        {
            return typeof(ClubMember);
        }

        public string getCreditCardDetials()
        {
            return CreditCardDetials;
        }

        public void setCreditCardDetials(string ccd)
        {
            CreditCardDetials = ccd;
        }
    }
}
