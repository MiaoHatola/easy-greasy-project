﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beckend.Entity.BasicEntities
{
    public enum Gender { Male, Female , Other }
    [Serializable]
    public abstract class Person : User
    {
        //private fields
        private int teudatZehut;
        private string firstName;
        private string lastName;
        private Gender gender;

        //public properties
        public int TeudatZehute
        {
            get
            {
                return this.teudatZehut;
            }
            set
            {
                this.teudatZehut = value;
            }
        }
        public string FirstName
        {
            get
            {
                return this.firstName;
            }
            set
            {
                this.firstName = value;
            }
        }
        public string LastName
        {
            get
            {
                return this.lastName;
            }
            set
            {
                this.lastName = value;
            }
        }
        public Gender Gender
        {
            get
            {
                return this.gender;
            }
        }
        public string EditableGender
        {
            set
            {
                this.gender = (Gender)Enum.Parse(typeof(Gender),value);
            }
        }
        
        //Constructur
        public Person(string userName,string password,string type,int teudatZehut, string firstName, string lastName, Gender gender): base(userName, password, type)
        {
            this.teudatZehut = teudatZehut;
            this.firstName = firstName;
            this.lastName = lastName;
            this.gender = gender;
        }
        public Person(string userName,string password,string type,string teudatZehut, string firstName, string lastName, string gender)
            : this(userName,password,type,int.Parse(teudatZehut), firstName, lastName, (Gender)Enum.Parse(typeof(Gender), gender)) { }
    }
}
