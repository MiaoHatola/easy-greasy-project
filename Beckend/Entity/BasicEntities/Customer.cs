﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beckend.Entity.BasicEntities
{
    [Serializable]
    public class Customer : User , IBuyer
    {
        private string creditCardDetials;

        public string CreditCardDetials
        {
            get
            {
                return creditCardDetials;
            }
            set
            {
                creditCardDetials = value;
            }
        }

        public Customer(string userName, string passWord, string type, string creditCardDetials)
            : base(userName, passWord, type)
        {
            this.creditCardDetials = creditCardDetials;
        }

        public override Type GetEntityType()
        {
            return typeof(Customer);
        }

        public string getCreditCardDetials()
        {
             return CreditCardDetials;  
        }

        public void setCreditCardDetials(string ccd)
        {
            CreditCardDetials = ccd;
        }
    }
}
