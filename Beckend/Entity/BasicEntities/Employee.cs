﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.DynamicQuerying;
using Beckend.DynamicQuerying.Queries;
using Beckend.DynamicQuerying.Conditions;

namespace Beckend.Entity.BasicEntities
{
    [Serializable]
    public class Employee : Person
    {
        //private fields
        private int departmentID;
        private int salary;
        private int supervisorID;

        //public proparties
        public int DepartmentID
        {
            get
            {
                return this.departmentID;
            }
            set
            {
                this.departmentID = value;
            }
        }
        public int Salary
        {
            get
            {
                return this.salary;
            }
            set
            {
                if (value < 0) throw new ArgumentException("salary cant be under zero");
                this.salary = value;
            }
        }
        public int SupervisorID
        {
            get
            {
                return this.supervisorID;
            }
            set
            {
                this.supervisorID = value;
            }
        }
        
        //Constracturs
        public Employee(string userName, string password, string type, string departmentID, string salary, string supervisorID, string teudatZehut, string firstName, string lastName, string gender)
            : this(userName, password, type, firstName, lastName, int.Parse(teudatZehut), int.Parse(departmentID), int.Parse(salary), int.Parse(supervisorID), (Gender)Enum.Parse(typeof(Gender), gender)) { }
        public Employee(string userName,string password,string type,string firstName, string lastName, int teudatZehut, int departmentID, int salary, int supervisorID, Gender gender)
            : base(userName, password, type,teudatZehut, firstName, lastName, gender)
        {
            this.departmentID = departmentID;
            this.supervisorID = supervisorID;
            this.salary = salary;
        }
        
        public override DynamicQuerying.Query[] GetDeleteActions()
        {
            EditQuery eq = new EditQuery();
            PropertyInfo pi = EntityHelper.GetPropertyByName(typeof(Employee), "DepartmentID");
            eq.SetWhere(new SingleCondition(pi, 0, departmentID.ToString()));
            eq.SetEdit(pi, "-1");

            return new Query[] { eq };
        }
        public override bool Delete()
        {
            return base.Delete();
        }
        public override Type GetEntityType()
        {
            return typeof(Employee);
        }
    }
}
