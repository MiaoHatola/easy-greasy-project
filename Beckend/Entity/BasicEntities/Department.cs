﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Beckend.DynamicQuerying;
using Beckend.DynamicQuerying.Queries;
using Beckend.DynamicQuerying.Conditions;

namespace Beckend.Entity.BasicEntities
{
    [Serializable]
    public class Department : DynamicEntity
    {
        //private fields
        private string departmentName;

        //public properties
        public int DepartmentID
        {
            get
            {
                return ID;
            }
        }
        public string DepartmentName
        {
            get
            {
                return this.departmentName;
            }
            set
            {
                this.departmentName = value;
            }
        }

        //Constracturs
        public Department(string departmentName)
        {
            this.departmentName = departmentName;
        }

        public override Type GetEntityType()
        {
            return typeof(Department);
        }
        public override Query[] GetDeleteActions()
        {
            EditQuery editEmployees = new EditQuery();
            DeleteQuery deleteProducts = new DeleteQuery();

            editEmployees.SetWhere(new SingleCondition(EntityHelper.GetPropertyByName(typeof(Employee), "DepartmentID"), 0, ID.ToString()));
            deleteProducts.SetWhere(new SingleCondition(EntityHelper.GetPropertyByName(typeof(Product), "Location"), 0, ID.ToString()));

            editEmployees.SetEdit(EntityHelper.GetPropertyByName(typeof(Employee), "DepartmentID"), "-1");

            return new Query[] { editEmployees, deleteProducts };
        }
        public override bool Delete()
        {
            base.Delete();
            return true;
        }
    }
}