﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beckend.Entity.BasicEntities
{
    [Serializable]
    public class Reciept
    {
        Dictionary<int, BoughtProduct> boughtPro;

        public BoughtProduct BoughtPro
        {
            set
            {
                boughtPro.Add(value.ProductId,value);
            }
        }

        public List<BoughtProduct> reciept
        {
            get
            {
                return boughtPro.Values.ToList();
            }
        }

        public Reciept()
        {
            boughtPro = new Dictionary<int, BoughtProduct>();
        }

        public void addProduct(BoughtProduct bp)
        {
            if (!boughtPro.Keys.Contains(bp.ProductId))
            {
                BoughtPro = bp;
            }
            else
            {
                this.boughtPro[bp.ProductId].addProducts(bp.ProductQuantity);
            }
        }

        public int reduceProduct(Product p, int quantity)
        {
            if (!boughtPro.Keys.Contains(p.ID)) return 0;
            else
            {
                int currentQuantity = this.boughtPro[p.ID].ProductQuantity;
                if (currentQuantity <= quantity)
                {
                    boughtPro.Remove(p.ID);
                    currentQuantity = p.StockCount;
                    return currentQuantity;
                }
                else
                {
                    boughtPro[p.ID].reduceProducts(quantity);
                    return quantity;
                }
            }
        }
        public int CalcPrice()
        {
            int price = 0;
            BoughtProduct[] bp = boughtPro.Values.ToArray();
            foreach (BoughtProduct product in bp)
            {
                price += product.ProductPrice * product.ProductQuantity;
            }
            return price;
        }
        public override string ToString()
        {
            return "Total Price: " + CalcPrice().ToString();
        }
    }
}
