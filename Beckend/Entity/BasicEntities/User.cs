﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beckend.Entity.BasicEntities
{
    [Serializable]
    public class User : DynamicEntity
    {
        //private fields
        private string username;
        private string password;
        private string type;

        //public proparties 
        public string Username
        {
            get
            {
                return this.username;
            }
            set
            {
                this.username = value;
            }
        }
        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
            }
        }
        public string Type
        {
            get
            {
                return this.type;
            }
            set
            {
                this.type = value;
            }
        }
        public User(string username, string password, string type)
        {
            this.username = username;
            this.password = password;
            this.type = type;
        }

        public override Type GetEntityType()
        {
            return typeof(User);
        }
    }
}
