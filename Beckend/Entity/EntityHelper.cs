﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.Entity.BasicEntities;
using Beckend.DynamicQuerying;

namespace Beckend.Entity
{
    public enum EntityType
    {
        ClubMember,
        Department,
        Employee,
        Product,
        Query,
        Transaction,
        Customer,
        BoughtProduct
    }
    public class EntityHelper
    {
        public static Array GetEntitys()
        {
            return Enum.GetValues(typeof(EntityType));
        }
        public static int CountEntitys()
        {
            return Enum.GetNames(typeof(EntityType)).Length;
        }
        public static FieldInfo[] GetFields(Type T)
        {
            return GetFields(T, true);
        }
        public static FieldInfo[] GetFields(Type T, bool recursively)
        {
            FieldInfo[] ans;
            int length = 0;
            if (recursively)
                GetFieldsRecursively(T, out ans, 0, ref length);
            else
                ans = T.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic);
            return ans;
        }
        private static void GetFieldsRecursively(Type T, out FieldInfo[] arr, int i, ref int length)
        {
            FieldInfo[] tmp = T.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic);
            i += tmp.Length;
            if (T != typeof(Object) && length == 0)
                GetFieldsRecursively(T.BaseType, out arr, i, ref length);
            else
            {
                arr = new FieldInfo[i];
                length = i;
            }
            tmp.CopyTo(arr, length - i);
        }
        private static PropertyInfo[] GetAllPropertys(Type T)
        {
            return T.GetProperties(BindingFlags.Public | BindingFlags.Instance);
        }
        public static PropertyInfo[] GetWritableProperties(Type T)
        {
            PropertyInfo[] tmp = GetAllPropertys(T);
            PropertyInfo[] ans;
            int l = tmp.Length;
            int j = 0;

            for (int i = 0; i < tmp.Length; i++)
                if (!tmp[i].CanWrite)
                {
                    tmp[i] = null;
                    l--;
                }
            
            ans = new PropertyInfo[l];

            for (int i = 0; i < tmp.Length; i++)
                if (tmp[i] != null)
                {
                    ans[j] = tmp[i];
                    j++;
                }
            return ans;
        }
        public static PropertyInfo[] GetReadableProperties(Type T)
        {
            PropertyInfo[] tmp = GetAllPropertys(T);
            PropertyInfo[] ans;
            int l = tmp.Length;
            int j = 0;

            for (int i = 0; i < tmp.Length; i++)
                if (!tmp[i].CanRead)
                {
                    tmp[i] = null;
                    l--;
                }

            ans = new PropertyInfo[l];

            for (int i = 0; i < tmp.Length; i++)
                if (tmp[i] != null)
                {
                    ans[j] = tmp[i];
                    j++;
                }
            return ans;
        }
        public static EntityType GetEnumFromInt(int n)
        {
            return (EntityType)n;
        }
        public static EntityType GetEnumFromString(string str)
        {
            return (EntityType)Enum.Parse(typeof(EntityType), str);
        }
        public static EntityType GetEnumFromShortString(string str)
        {
            Array arr = Enum.GetValues(typeof(EntityType));
            
            for (int i = 0; i < arr.Length; i++)
            {
                string[] tmp = arr.GetValue(i).ToString().Split('.');
                if (tmp[tmp.Length - 1].Equals(str))
                    return (EntityType)arr.GetValue(i);
            }
            throw new Exception(str + " was not found in EntityType");
        }
        public static string GetNameFromType(Type t)
        {
            string[] tmp = t.ToString().Split('.');
            return tmp[tmp.Length - 1];
        }
        public static Type GetTypeFromEnum(EntityType et)
        {
            switch (et)
            {
                case EntityType.ClubMember: return typeof(ClubMember);
                case EntityType.Department: return typeof(Department);
                case EntityType.Employee: return typeof(Employee);
                case EntityType.Product: return typeof(Product);
                case EntityType.Transaction: return typeof(Transaction);
                case EntityType.Customer: return typeof(Customer);
                case EntityType.Query: return typeof(Query);
                case EntityType.BoughtProduct: return typeof(BoughtProduct);
                default: throw new ArgumentException("no entity type was chosen (EntityHelper)");
            }
        }
        public static ParameterInfo[] GetConstructorParameters(Type T)
        {
            ConstructorInfo[] ci = T.GetConstructors();
            ParameterInfo[] pi;

            bool isRight = true;

            for (int i = 0; i < ci.Length; i++)
            {
                pi = ci[i].GetParameters();
                for (int j = 0; j < pi.Length; j++)
                    if (pi[0].ParameterType != typeof(string))
                        isRight = false;
                if (isRight)
                    return pi;
                else
                    isRight = true;
            }
            return null;
        }
        public static Type GetTypeFromString(string str)
        {
            return GetTypeFromEnum(GetEnumFromString(str));
        }
        public static Type GetTypeFromShortString(string str)
        {
            return GetTypeFromEnum(GetEnumFromShortString(str));
        }
        public static Type GetTypeFromInt(int n)
        { 
            return GetTypeFromEnum(GetEnumFromInt(n));
        }
        public static string GetEntityNamesFromEnum()
        {
            string o = "Entity ";
            string ans = "";
            string[] tmp;
            for (int i = 0; i < Enum.GetNames(typeof(EntityType)).Length; i++)
            {
                tmp = GetEnumFromInt(i).ToString().Split('.');
                ans +=o+" "+ (i + 1) + ": " + tmp[tmp.Length - 1] + "\n";
            }
            return ans;
        }
        public static FieldInfo GetFieldByName(Type T, string name)
        {
            FieldInfo[] fi = GetFields(T);
            for (int i = 0; i < fi.Length; i++)
            {
                if (fi[i].Name.Equals(name))
                {
                    return fi[i];
                }
            }
            return null;
        }
        public static PropertyInfo GetPropertyByName(Type T, string name)
        {
            PropertyInfo[] pi = GetAllPropertys(T);
            for (int i = 0; i < pi.Length; i++)
            {
                if (pi[i].Name.Equals(name))
                {
                    return pi[i];
                }
            }
            return null;
        }
        public static string PrintProperties(PropertyInfo[] pi, bool addLineNumber)
        {
            string ans = "";
            for (int i = 0; i < pi.Length; i++)
            {
                if (addLineNumber)
                    ans += i + ". ";
                ans += pi[i].Name + "\t(" + pi[i].PropertyType.ToString().Split('.').Last() + ")\n";
            }
            return ans;
        }
        public static string PrintParameters(ParameterInfo[] pi, bool addLineNumber)
        {
            string ans = "";
            for (int i = 0; i < pi.Length; i++)
            {
                if (addLineNumber)
                    ans += i + ". ";
                ans += pi[i].Name + "\t(" + pi[i].ParameterType.ToString().Split('.').Last() + ")\n";
            }
            return ans;
        }
        public static FieldInfo[] RemoveIDProperty(FieldInfo[] fi)
        {
            List<FieldInfo> ans = fi.ToList();
            for (int i = 0; i < fi.Length; i++)
                if (fi[i].Name.ToUpper().Equals("ID"))
                {
                    ans.Remove(fi[i]);
                    return ans.ToArray();
                }
            return ans.ToArray();
        }
        public static object[] GetObjectsFromFields(string className, string[] fieldNames, List<string[]> fieldValues)
        {
            List<object> builtObjects = new List<object>();
            
            Type T = GetTypeFromShortString(className);
            ParameterInfo[] ci = GetConstructorParameters(T);
            int[] filter = new int[fieldNames.Length];
            string[] activatorParameters = new string[ci.Length];

            int idIndex = -1;
            DynamicEntity de;
            // find ID
            for (int i = 0; i < fieldNames.Length; i++)
            {
                if (fieldNames[i].ToString() == "ID")
                {
                    idIndex = i;
                    break;
                }
            }
            // initialize filter
            for (int i = 0; i < ci.Length; i++)
            {
                for (int j = 0; j < fieldNames.Length; j++)

                    if (fieldNames[j].ToUpper() == ci[i].Name.ToUpper())
                    {
                        filter[i] = j;
                        break;
                    }
            }
            // build the objects
            foreach (string[] item in fieldValues)
            {
                for (int i = 0; i < activatorParameters.Length; i++)
                    activatorParameters[i] = item[filter[i]];
                de = Activator.CreateInstance(T, activatorParameters) as DynamicEntity;
                de.SetId(int.Parse(item[idIndex]));
                builtObjects.Add(de);
            }
            return builtObjects.ToArray();
        }
    }
}