﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.DynamicQuerying;

namespace Beckend.Entity
{
    [Serializable]
    public abstract class DynamicEntity
    {
        protected int id=-1;

        public int ID
        {
            get
            {
                return this.id;
            }
        }

        public void SetId(int id)
        {
            this.id = id;
        }

        public bool IsDeleted()
        {
            return false;
        }
        public virtual bool Delete()
        {
            return true;
        }
        public virtual Query[] GetDeleteActions()
        {
            return null;
        }
        public abstract Type GetEntityType();
    }
}
