﻿using System;
using NUnit.Framework;
using Beckend.DynamicQuerying.Queries;
using Beckend.DynamicQuerying.Conditions;
using Beckend.Entity;
using Beckend.Entity.BasicEntities;
using Database;
using Logic;
using System.Reflection;

namespace Testing
{
    [TestFixture]
    public class Tests
    {
        static string[] dataNames = new string[] { "Product", "Department", "ClubMember", "Customer", "Transaction", "Employee" };
        [Test]
        public void Test1()
        {
            for (int i = 0; i < dataNames.Length; i++)
            {
                SelectQuery sq = new SelectQuery();
                sq.SetDataName(dataNames[i]);
                sq.SetWhere(new AllCondition());
                Assert.AreEqual("SELECT * FROM ["+dataNames[i]+"]", sq.GetSQLCommand()); 
            }
        }
        [Test]
        public void Test2()
        {
            for (int i = 0; i < dataNames.Length; i++)
            {
                EditQuery eq = new EditQuery();
                eq.SetDataName(dataNames[i]);
                eq.SetWhere(new AllCondition());
                eq.SetEdit(EntityHelper.GetWritableProperties(EntityHelper.GetTypeFromShortString(dataNames[i])), new string[]{});
                Assert.AreEqual("UPDATE "+dataNames[i]+" SET ", eq.GetSQLCommand());
            }
        }
        [Test]
        public void Test3()
        {
            for (int i = 0; i < dataNames.Length; i++)
            {
                DeleteQuery dq = new DeleteQuery();
                dq.SetDataName(dataNames[i]);
                dq.SetWhere(new AllCondition());
                Assert.AreEqual("DELETE [" + dataNames[i] + "] FROM "+dataNames[i], dq.GetSQLCommand());
            }
        }
        [Test]
        public void Test4()
        {
            InsertQuery iq = new InsertQuery();
            iq.SetDataName("ClubMember");
            ClubMember cm = new ClubMember("User","Pass","Admin","First","Last","316",DateTime.Now.Date.ToString(),"Male","1274018");
            iq.SetInsert(cm);
            string ans = iq.GetSQLCommand();
            Assert.AreEqual("INSERT INTO [ClubMember] (USERNAME, PASSWORD, TYPE, TEUDATZEHUT, FIRSTNAME, LASTNAME, GENDER, DATEOFBIRTH, CREDITCARDDETIALS) VALUES ('User', 'Pass', 'Admin', 316, 'First', 'Last', 'Male', '" + DateTime.Now.Date.ToString()+ "', '1274018') ", ans);

        }
        [Test]
        public void Test5()
        {
            DatabaseTier dbt = new DatabaseTier();
            SelectQuery sq = new SelectQuery();
            sq.SetDataName("asgas;vjb");
            sq.SetWhere(new AllCondition());
            dbt.SubmitQuery(sq);
            Assert.IsNull(dbt.RunQuery());
        }
        [Test]
        public void Test6()
        {
            for (int i = 0; i < dataNames.Length; i++)
            {
                Assert.IsNotNull(EntityHelper.GetTypeFromShortString(dataNames[i]));
            }
        }
        [Test]
        public void Test7()
        {
            Assert.IsNotNull(LogicTier.GetInstance());
        }
        [Test]
        public void Test8()
        {
            SelectQuery sq1 = new SelectQuery();
            SelectQuery sq2 = new SelectQuery();
            sq1.SetDataName("Employee");
            sq2.SetDataName("Employee");
            sq1.SetWhere(new SingleCondition("ID > 5", EntityHelper.GetPropertyByName(EntityHelper.GetTypeFromShortString("Employee"), "ID")));
            sq2.SetWhere(new SingleCondition("ID < 5", EntityHelper.GetPropertyByName(EntityHelper.GetTypeFromShortString("Employee"), "ID")));
            Assert.AreNotEqual(LogicTier.GetInstance().RunQuery(sq1), LogicTier.GetInstance().RunQuery(sq2));
        }
        [Test]
        public void Test9()
        {
            EntityType[] types = ((EntityType[])EntityHelper.GetEntitys());
            string typeString = "";
            for (int i = 0; i < types.Length; i++)
            {
                typeString += types[i].ToString();
            }
            Assert.AreEqual("ClubMemberDepartmentEmployeeProductQueryTransactionCustomerBoughtProduct", typeString);
        }

        [Test]
        public void Test10()
        {
            FieldInfo[] fi = EntityHelper.GetFields(typeof(Product));
            string fieldsString = "";
            for (int i = 0; i < fi.Length; i++)
            {
                fieldsString += ((fi[i].ToString()).Split(' '))[1];
            }
            Assert.AreEqual("idproductNameproductTypeinventoryIDlocationpricestockCountamountOfSales", fieldsString);
        }
        [Test]
        public void Test11()
        {
            int length = EntityHelper.GetEntitys().Length;
            string[] names = new string[] { "Product", "Employee", "Department", "Customer", "ClubMember", "Transaction" };
            Assert.AreEqual(typeof(Product), EntityHelper.GetTypeFromShortString(dataNames[0]));
            Assert.AreEqual(typeof(Department), EntityHelper.GetTypeFromShortString(dataNames[1]));
            Assert.AreEqual(typeof(ClubMember), EntityHelper.GetTypeFromShortString(dataNames[2]));
            Assert.AreEqual(typeof(Customer), EntityHelper.GetTypeFromShortString(dataNames[3]));
            Assert.AreEqual(typeof(Transaction), EntityHelper.GetTypeFromShortString(dataNames[4]));
            Assert.AreEqual(typeof(Employee), EntityHelper.GetTypeFromShortString(dataNames[5]));
        }
        [Test]
        public void Test12()
        {
            Assert.AreNotEqual(EntityHelper.GetWritableProperties(typeof(Product)), EntityHelper.GetReadableProperties(typeof(Product)));
            Assert.AreNotEqual(EntityHelper.GetWritableProperties(typeof(Customer)), EntityHelper.GetReadableProperties(typeof(Customer)));
            Assert.AreNotEqual(EntityHelper.GetWritableProperties(typeof(ClubMember)), EntityHelper.GetReadableProperties(typeof(ClubMember)));
            Assert.AreNotEqual(EntityHelper.GetWritableProperties(typeof(Transaction)), EntityHelper.GetReadableProperties(typeof(Transaction)));
            Assert.AreNotEqual(EntityHelper.GetWritableProperties(typeof(Employee)), EntityHelper.GetReadableProperties(typeof(Employee)));
        }
        [Test]
        public void Test13()
        {
            SelectQuery sq = new SelectQuery();
            sq.SetDataName("Product");
            sq.SetWhere(new SingleCondition("ProductName = Banaynay", EntityHelper.GetPropertyByName(EntityHelper.GetTypeFromShortString("Product"), "ProductName")));
            InsertQuery iq = new InsertQuery();
            iq.SetDataName("Product");
            Product p = new Product("Banaynay","10", "Meat","3","0","100","0");
            iq.SetInsert(p);
            object[] ans1 = LogicTier.GetInstance().RunQuery(sq);
            LogicTier.GetInstance().RunQuery(iq);
            object[] ans2 = LogicTier.GetInstance().RunQuery(sq);
            Assert.AreNotEqual(ans1.Length, ans2.Length);
            Assert.AreEqual(ans1.Length+1, ans2.Length);
        }
        [Test]
        public void Test14()
        {
            InsertQuery iq = new InsertQuery();
            iq.SetDataName("Product");
            Product p = new Product("Banaynay", "10", "Meat", "3", "0", "100", "0");
            iq.SetInsert(p);
            LogicTier.GetInstance().RunQuery(iq);
            EditQuery eq = new EditQuery();
            eq.SetWhere(new SingleCondition("ProductName = Banaynay", EntityHelper.GetPropertyByName(EntityHelper.GetTypeFromShortString("Product"), "ProductName")));
            eq.SetEdit(EntityHelper.GetPropertyByName(EntityHelper.GetTypeFromShortString("Product"), "ProductName"), "Banaynot");
            LogicTier.GetInstance().RunQuery(eq);

            SelectQuery sq = new SelectQuery();
            sq.SetDataName("Product");
            sq.SetWhere(new SingleCondition("ProductName = Banaynay", EntityHelper.GetPropertyByName(EntityHelper.GetTypeFromShortString("Product"), "ProductName")));
            object[] obj = LogicTier.GetInstance().RunQuery(sq);
            Assert.AreNotEqual((Product)obj[0],(Product)p);
        }
        [Test]
        public void Test15()
        {
            Assert.AreEqual(EntityHelper.GetFields(typeof(Product)), EntityHelper.GetFields(EntityHelper.GetTypeFromShortString("Product")));
        }
        [Test]
        public void Test16()
        {
            Assert.AreNotEqual(EntityHelper.RemoveIDProperty(EntityHelper.GetFields(typeof(Product))), EntityHelper.GetFields(typeof(Product)));
        }
        [Test]
        public void Test17()
        {
            Assert.AreNotEqual(EntityHelper.RemoveIDProperty(EntityHelper.GetFields(typeof(Employee))), EntityHelper.GetFields(typeof(Employee)));
        }
        [Test]
        public void Test18()
        {
            ILogicStepper logic = LogicTier.GetInstance();
            object[] obj = logic.GetAllData("Product");
            logic.DeleteObjects("Product", obj);
            Assert.IsEmpty(logic.GetAllData("Product"));
            logic.Save("Product", obj);
        }
        [Test]
        public void Test19()
        {
            ILogicStepper logic = LogicTier.GetInstance();
            object[] obj = logic.GetAllData("Product");
            logic.DeleteObjects("Product", obj);
            Assert.IsEmpty(logic.GetAllData("Product"));
            logic.Save("Product",obj);
            Assert.AreEqual(obj,logic.GetAllData("Product"));
        }
        [Test]
        public void Test20()
        {
            SelectQuery sq = new SelectQuery();
            sq.SetDataName("Product");
            PropertyInfo[] Properties = EntityHelper.GetReadableProperties(typeof(Product));
            sq.SetWhere(new AndCondition("ID > 0 & Price > 5" ,Properties));
            Assert.AreEqual("SELECT * FROM [Product] WHERE ID>'0' AND PRICE>'5'", sq.GetSQLCommand());
        }
    }
}

