﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Logic;
using Beckend.Entity;
using Beckend.Entity.BasicEntities;
using Beckend.DynamicQuerying;
using UserInterface.Windows;
using System.Windows;

namespace UserInterface
{
    public class Globals
    {
        /*
         * This class provides global information about runtime user and window managment.
         * it contains static fields that assist other parts of the program to communicate better.
         */
        private static List<Query> QueryHistory = new List<Query>();
        private static Window profile;
        public static ILogicStepper LogicInstance { get { return LogicTier.GetInstance(); } }
        public static User CurrentUser { get; set; }
        public static int queryID = 0;

        public static void InitializeLogic()
        {
            var v = LogicInstance;
        }
        public static void AddQuery(Query q)
        {
            q.SetId(queryID);
            queryID++;
            QueryHistory.Add(q);
        }
        public static List<Query> GetQueryHistory()
        {
            return QueryHistory;
        }
        public static string[] GetCurrentUserInfo()
        {
            string[] info = new string[2];
            PropertyInfo[] pi = EntityHelper.GetReadableProperties(typeof(User));
            for (int i = 0; i < pi.Length; i++)
            {
                if (pi[i].Name != "Password")
                {
                    info[0] += pi[i].Name + ":\n";
                    info[1] += pi[i].GetValue(CurrentUser) + "\n";
                }
            }
            return info;
        }
        public static string[] GetInfoOf(DynamicEntity obj)
        {
            string[] info = new string[2];
            PropertyInfo[] pi = EntityHelper.GetReadableProperties(obj.GetEntityType());
            for (int i = 0; i < pi.Length; i++)
            {
                if (pi[i].Name != "Password")
                {
                    info[0] += pi[i].Name + ":\n";
                    info[1] += pi[i].GetValue(obj) + "\n";
                }
            }
            return info;
        }
        public static void OpenCurrentProfile()
        {
            /*
             * The next section sends the user to the approperiate window.
             */
            if (profile == null || !profile.IsActive)
            {
                if (CurrentUser.Type.Equals("Admin"))
                    profile = new AdminWindow();
                else if (CurrentUser.Type.Equals("Manager"))
                    profile = new ManagerWindow();
                else if (CurrentUser.Type.Equals("Worker"))
                    profile = new WorkerWindow();
                else if (CurrentUser.Type.Equals("Customer"))
                    profile = new CustomerWindow();
                else profile = new RegisterWindow();
            }
            profile.Show();
        }
        public static void LogOut()
        {
            CurrentUser = null;
            new LoginWindow().Show();
            if (profile != null)
                profile.Close();
        }
    }
}
