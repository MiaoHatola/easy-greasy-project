﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Web;
using Beckend.Weather;

namespace UserInterface.Windows.Wizard
{
    /// <summary>
    /// Interaction logic for WeatherTest.xaml
    /// </summary>
    public partial class ShopProfile : MahApps.Metro.Controls.MetroWindow
    {
        private int i;
        private string location;
        private List<WeatherDetails> wd;

        public ShopProfile(string location)
        {
            InitializeComponent();

            i = 0;
            this.location = location;
            MapContainer.Visibility = Visibility.Hidden;
            string path = Directory.GetCurrentDirectory();
            path = path.Substring(0, path.Length - "bin\\Debug\\".Length) + "\\Windows\\Wizard\\MapPage.html";

            var uriBuilder = new UriBuilder(path);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["q"] = location;
            uriBuilder.Query = query.ToString();
            path = uriBuilder.ToString();

            MapContainer.Navigate(path);
            MapContainer.Visibility = Visibility.Visible;
            wd = WeatherHelper.GetWeather(location);
            RefreshDetails();
        }
        private void RefreshDetails()
        {
            this.Title = location;
            if (wd.Count > 0)
            {
                WeatherDetails details = wd.ElementAt(this.i);
                Day.Text = details.WeatherDay;
                WeatherCast.Text = details.Weather;
                Temperature.Text = details.Temperature;
                Humidity.Text = details.Humidity;
                MinTemp.Text = details.MinTemperature;
                MaxTemp.Text = details.MaxTemperature;
                WindSpeed.Text = details.WindSpeed;
                WindDirection.Text = details.WindDirection;
                WeaterIcon.Source = new BitmapImage(new Uri("/UserInterface;component/Design/" + details.WeatherIcon, UriKind.Relative));
            }
        }

        private void PreviousDay_Click(object sender, RoutedEventArgs e)
        {
            if (i > 0)
            {
                i--;
                RefreshDetails();
            }
        }

        private void NextDay_Click(object sender, RoutedEventArgs e)
        {
            if (i < wd.Count - 1)
            {
                i++;
                RefreshDetails();
            }
        }
    }
}