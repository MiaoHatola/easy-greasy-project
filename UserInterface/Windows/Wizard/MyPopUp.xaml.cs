﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UserInterface.Windows.Wizard
{
    /// <summary>
    /// Interaction logic for MyPopUp.xaml
    /// </summary>
    public partial class MyPopUp : MahApps.Metro.Controls.MetroWindow
    {
        public MyPopUp(string msg)
        {
            InitializeComponent();
            textblock.Text = msg;
        }

        private void OK_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
