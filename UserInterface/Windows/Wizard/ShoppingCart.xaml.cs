﻿using Beckend.DynamicQuerying.Conditions;
using Beckend.DynamicQuerying.Queries;
using Beckend.Entity;
using Beckend.Entity.BasicEntities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Threading;

namespace UserInterface.Windows.Wizard
{
    /// <summary>
    /// The window in charge of creating a purchase. has a "Drag&Drop" feature for easy purchases.
    /// this window also features a progress bar.
    /// </summary>
    public partial class ShoppingCart : MahApps.Metro.Controls.MetroWindow
    {
        private string input;
        private IBuyer buyer;
        private Product selectedProduct;
        private ObservableCollection<Product> prods;
        private Department[] deps;
        private Reciept bought = new Reciept();
        private ObservableCollection<BoughtProduct> oBought = new ObservableCollection<BoughtProduct>();
        private int prevRowIndex = -1;
        private int progress;
        private int max;
        private bool paid;
        private bool Stock = true;

        BackgroundWorker imageOpacity;

        private BackgroundWorker prog_worker;

        public ShoppingCart()
        {
            InitializeComponent();
            this.ProductsDataGrid.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(ProductsDataGrid_PreviewMouseLeftButtonDown);
            this.buyer = Globals.CurrentUser as IBuyer;

            this.input = buyer.getCreditCardDetials();

            departments.Items.Add("All");

            // initialize products
            object[] tmp = Globals.LogicInstance.GetAllData("Product");
            prods = castToProducts(tmp);            
            if (prods.Count != 0)
            {
                // initialize departments
                tmp = Globals.LogicInstance.GetAllData("Department");
                deps = new Department[tmp.Length];
                for (int i = 0; i < tmp.Length; i++)
                {
                    deps[i] = tmp[i] as Department;
                    departments.Items.Add(deps[i].DepartmentName);
                }
                departments.SelectedIndex = 0;
                ProductsDataGrid.IsReadOnly = true;

                imageOpacity = new BackgroundWorker();
                imageOpacity.WorkerReportsProgress = true;
                imageOpacity.DoWork += animateArrow;
                imageOpacity.ProgressChanged += updateArrowTransparency;

                progress = 0;
                paid = false;

                prog_worker = new BackgroundWorker();
                prog_worker.WorkerReportsProgress = true;
                prog_worker.DoWork += worker_DoWork;
                prog_worker.ProgressChanged += worker_ProgressChanged;
                prog_worker.RunWorkerAsync();

                new MyPopUp("Be sure to check out our best seller this month: " + prods.First().ProductName + "!").ShowDialog();
            }
            else
            {
                Stock = false;
            }
        }
        /// <summary>
        /// The following part is responsible for managing and updating the progress bar using a backgroung worker.
        /// this enables us real time tracking of the dataGrid build process and, as a result, a functional progress bar.
        /// </summary>
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (max != 0)
                    (sender as BackgroundWorker).ReportProgress(100*progress/max);
                else
                    (sender as BackgroundWorker).ReportProgress(100);
                Thread.Sleep(1000);
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Progi.Value = e.ProgressPercentage;
        }

        //Initiates the filtering progress - based on department chosen.
        private void departments_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox depa = (ComboBox)sender;
            if (((string)depa.SelectedValue).Equals("All"))
            {
                ProductsDataGrid.ItemsSource = this.prods;
            }
            else
            {
                string depatmentName = (string)depa.SelectedValue;
                int departmentID = -1;
                foreach (Department dep in deps)
                {
                    if (dep.DepartmentName.Equals(depatmentName))
                    {
                        departmentID = dep.DepartmentID;
                        break;
                    }
                }
                if (departmentID != -1)
                {
                    SelectQuery products = new SelectQuery();

                    products.SetWhere(new SingleCondition(EntityHelper.GetPropertyByName(typeof(Product), "Location"), 0, departmentID.ToString()));
                    products.SetQuerable(prods.ToList<DynamicEntity>());
                    ProductsDataGrid.ItemsSource = castToProducts(products.RunQuery());
                }
            }
        }

        //private method for casting an object array into a Observable collection.
        private ObservableCollection<Product> castToProducts(object[] arr)
        {
            List<Product> ans = new List<Product>();
            progress = 0;
            max = arr.Length;
            for (int i = 0; i < arr.Length; i++)
            {
                if ((arr[i] as Product).InStock)
                    ans.Add(arr[i] as Product);
                Thread.Sleep(10);
                progress++;
            }
            ans.Sort();
            return new ObservableCollection<Product>(ans);
        }

        //Contionus to the credit card input box, and then checkout.
        private void Buy_Click(object sender, RoutedEventArgs e)
        {
            if (bought.reciept.Count > 0)
            {
                InputTextBox.Text = input;
                InputBox.Visibility = System.Windows.Visibility.Visible;
            }
        }

        public delegate Point GetDragDropPosition(IInputElement element);
        //Drag&Drop - checks if the mouse is on the Drag&Drop target.
        private bool IsTheMouseOnTargetRow(Visual theTarget, GetDragDropPosition pos)
        {
            if (theTarget == null)
                return false;
            Rect posBounds = VisualTreeHelper.GetDescendantBounds(theTarget);
            Point theMousePos = pos((IInputElement)theTarget);
            return posBounds.Contains(theMousePos);
        }

        //Drag&Drop - gets the dragged item.
        private DataGridRow GetDataGridRowItem(int index, DataGrid dg)
        {
            if (dg.ItemContainerGenerator.Status != GeneratorStatus.ContainersGenerated)
                return null;

            return dg.ItemContainerGenerator.ContainerFromIndex(index) as DataGridRow;
        }

        //Drag&Drop - gets the item by index.
        private int GetDataGridItemCurrentRowIndex(GetDragDropPosition pos, DataGrid dg)
        {
            int curIndex = -1;
            for (int i = 0; i < dg.Items.Count; i++)
            {
                DataGridRow itm = GetDataGridRowItem(i, dg);
                if (IsTheMouseOnTargetRow(itm, pos))
                {
                    curIndex = i;
                    break;
                }

            }
            return curIndex;
        }

        //Drag&Drop - drops the item in the 'bought' items datagrid
        private void SelledProductsDataGrid_Drop(object sender, DragEventArgs e)
        {
            try
            {
                selectedProduct = (e.Data.GetData(typeof(Product)) as Product);
                NumericUpDownDialog nUDD = new NumericUpDownDialog(selectedProduct.StockCount, 1, "Please choose quantity for " + selectedProduct.ProductName + " (no more than " + selectedProduct.StockCount + ").", this);
                nUDD.Show();
            }
            catch
            { }
        }

        //Drag&Drop - updates and refreshes the datagrid after the wanted quantity has been set
        public void GetWantedQuantity(int quantity)
        {
            BoughtProduct wantedProduct = new BoughtProduct(selectedProduct, quantity);
            bought.addProduct(wantedProduct);
            oBought = new ObservableCollection<BoughtProduct>(bought.reciept);
            SelledProductsDataGrid.ItemsSource = oBought;
            selectedProduct.AmountOfSales += quantity;
            selectedProduct.StockCount = selectedProduct.StockCount - quantity;

            ProductsDataGrid.Items.Refresh();

            imageOpacity.RunWorkerAsync();
        }
        private void animateArrow(object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i < 100; i++)
            {
                imageOpacity.ReportProgress(100 - i);
                Thread.Sleep(5);
            }
            for (int i = 0; i < 100; i++)
            {
                imageOpacity.ReportProgress(i);
                Thread.Sleep(5);
            }
        }
        private void updateArrowTransparency(object sender, ProgressChangedEventArgs e)
        {
            ArrowImage.Opacity = e.ProgressPercentage/100.0;
        }
        //Drag&Drop - handles the mouse left button down event.
        private void ProductsDataGrid_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            prevRowIndex = GetDataGridItemCurrentRowIndex(e.GetPosition, ProductsDataGrid);

            if (prevRowIndex < 0)
                return;
            ProductsDataGrid.SelectedIndex = prevRowIndex;

            Product product = ProductsDataGrid.Items[prevRowIndex] as Product;

            if (product == null)
                return;

            DragDropEffects dragdropeffects = DragDropEffects.Move;

            if (DragDrop.DoDragDrop(ProductsDataGrid, product, dragdropeffects) != DragDropEffects.None)
            {
                ProductsDataGrid.SelectedItem = product;
            }
        }

        //confirms the usercredit card input, commits the purchase (Database updates, stock count etc.)
        private void YesButton_Click(object sender, RoutedEventArgs e)
        {
            input = InputTextBox.Text;
            if (input.Equals(""))
                return;

            InputBox.Visibility = System.Windows.Visibility.Collapsed;
            

            InsertQuery newTransaction = new InsertQuery();
            newTransaction.SetDataName("Transaction");
            int x = Globals.CurrentUser.ID;
            Transaction newT = new Transaction(false, PaymentMethod.CreditCard, bought.CalcPrice(), Globals.CurrentUser.ID, DateTime.Now);
            newTransaction.SetInsert(newT);
            object[] t = Globals.LogicInstance.RunQuery(newTransaction);

            if (SaveAsDefault.IsChecked.Value)
                buyer.setCreditCardDetials(input);

            foreach (BoughtProduct pro in bought.reciept)
            {
                pro.TransactionID = (t[0] as Transaction).ID;
            }

            Globals.LogicInstance.Save("ClubMember", buyer);
            Globals.LogicInstance.Save("Product", prods.ToArray());
            Globals.LogicInstance.Save("BoughtProduct", bought.reciept.ToArray());
            paid = true;
            this.Close();
        }

        private void ProductsDataGrid_Drop(object sender, DragEventArgs e)
        {
            try
            {
                BoughtProduct selectedProduct = (e.Data.GetData(typeof(BoughtProduct)) as BoughtProduct);
                Product pr = FindProduct(selectedProduct);
                if (pr != null)
                {
                    pr.StockCount += selectedProduct.ProductQuantity;
                    pr.AmountOfSales -= selectedProduct.ProductQuantity;
                }
                oBought.Remove(selectedProduct);
                bought.reduceProduct(pr, selectedProduct.ProductQuantity);

                // refresh grids
                SelledProductsDataGrid.ItemsSource = null;
                SelledProductsDataGrid.ItemsSource = oBought;
                ProductsDataGrid.ItemsSource = null;
                ProductsDataGrid.ItemsSource = prods;
            }
            catch { }
        }

        private Product FindProduct(BoughtProduct selectedProduct)
        {
            return (Product)prods.Single(p => p.ID == selectedProduct.ProductId);
        }

        private void SelledProductsDataGrid_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            int prevRowIndex = GetDataGridItemCurrentRowIndex(e.GetPosition, SelledProductsDataGrid);

            if (prevRowIndex < 0)
                return;
            SelledProductsDataGrid.SelectedIndex = prevRowIndex;

            BoughtProduct product = SelledProductsDataGrid.Items[prevRowIndex] as BoughtProduct;

            if (product == null)
                return;

            DragDropEffects dragdropeffects = DragDropEffects.Move;

            if (DragDrop.DoDragDrop(SelledProductsDataGrid, product, dragdropeffects) != DragDropEffects.None)
            {
                SelledProductsDataGrid.SelectedItem = product;
            }
        }

        private void ShoppingCartWindow_Closing(object sender, CancelEventArgs e)
        {
            if (!paid)
            {
                Product tmp;
                foreach (BoughtProduct bp in bought.reciept)
                {
                    tmp = FindProduct(bp);
                    if (tmp != null)
                        tmp.StockCount += bp.TransactionID;
                }
            }
        }

        internal bool hasProducts()
        {
            return Stock;
        }
    }
}
