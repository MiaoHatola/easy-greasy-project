﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Beckend.DynamicQuerying;
using Beckend.DynamicQuerying.Queries;
using Beckend.DynamicQuerying.Conditions;
using Beckend.DynamicQuerying.Attributes;
using Logic;

namespace UserInterface.Windows.Wizard
{
    /// <summary>
    /// This window guides the user through the process of creating a query, and sends that query to the database when finished.
    /// </summary>
    public partial class QueryBuilder : MahApps.Metro.Controls.MetroWindow
    {
        private ILogicStepper lt;
        private List<object> lst;
        bool first;
        bool isInsert;
        List<Window> children = new List<Window>();

        public QueryBuilder()
        {
            InitializeComponent();
            lt = LogicTier.GetInstance();
            Menu.Text = "Please select the query you would like to run:";
            OptionsContainer.ItemsSource = lst = arrToRadio(LogicTier.options);
            first = true;
            isInsert = false;
        }

        //creates checkbox from array
        private List<object> arrToCheckBox(object[] arr)
        {
            List<object> lst = new List<object>();
            CheckBox tmp;
            for (int i = 0; i < arr.Length; i++)
            {
                tmp = new CheckBox();
                tmp.Content = arr[i].ToString();
                lst.Add(tmp);
            }
            return lst;
        }

        //creates radio buttons from array
        private List<object> arrToRadio(object[] arr)
        {
            List<object> lst = new List<object>();
            RadioButton tmp;
            for (int i = 0; i < arr.Length; i++)
            {
                if (!isInsert || arr[i].ToString() != "Transaction")
                {
                    tmp = new RadioButton();
                    tmp.Content = arr[i].ToString();
                    tmp.GroupName = "OptionButtons";
                    lst.Add(tmp);
                }
            }
            ((RadioButton)lst.First()).IsChecked = true;
            return lst;
        }

        //Prints the user's current options
        private void PrintOptions()
        {
            Menu.Text = lt.GetMenu();
            switch (lt.GetStepType())
            {
                case StepAttribute.StepOptionType.MultiOptional:
                    OptionsContainer.ItemsSource = lst = arrToCheckBox(lt.GetOptions());
                    break;
                case StepAttribute.StepOptionType.SingleOption:
                    OptionsContainer.ItemsSource = lst = arrToRadio(lt.GetOptions());
                    break;
                default: lst = new List<object>();
                    TextBox tmp = new TextBox();
                    tmp.ClipToBounds = false;
                    tmp.Width = OptionsContainer.Width * 3/4;
                    lst.Add(tmp);
                    OptionsContainer.ItemsSource = lst;
                    break;
            }
        }
        /*
         * Next Button's click events
         */
        private void Next_Click(object sender, RoutedEventArgs e)
        {
            if (first)
            {
                first = false;
                for (int i = 0; i < lst.Count; i++)
                {
                    if (((RadioButton)lst[i]).IsChecked.Value)
                    {
                        if (((RadioButton)lst[i]).Content.ToString().ToUpper() == "INSERT")
                            isInsert = true;
                        lt.StartSteps(i);
                        PrintOptions();
                        return;
                    }
                }
            }
            Build();
            if (lt.IsFinished())
            {
                this.Hide();
                DisplayObjects window = new DisplayObjects(true, lt.GetResults());
                Globals.AddQuery(lt.GetBuiltQuery());
                window.Show();
                onClose();
                this.Close();
            }
            else
                PrintOptions();
        }
        /*
         * Builds the query using the StepBuilder logic abilities 
         */
        private void Build()
        {
            int[] indexes = new int[lst.Count];
            int j = 0;

            switch (lt.GetStepType())
            {
                case StepAttribute.StepOptionType.MultiOptional:
                    {
                        for (int i = 0; i < indexes.Length; i++)
                        {
                            if (((CheckBox)lst[i]).IsChecked.Value)
                            {
                                indexes[j] = i;
                                j++;
                            }
                        }
                        int[] arr = new int[j];
                        for (int i = 0; i < j; i++)
                        {
                            arr[i] = indexes[i];
                        }
                        lt.Build(arr);
                    }
                    break;
                case StepAttribute.StepOptionType.SingleOption:
                    {
                        for (int i = 0; i < lst.Count; i++)
                        {
                            if (((RadioButton)lst[i]).IsChecked.Value)
                                lt.Build(i);
                        }
                    }
                    break;
                default: lt.Build(((TextBox)lst.First()).Text);
                    break;
            }
        }
        
        //goes back one step (if available) in the step builder.
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            if (first)
                this.Close();
            else if (lt.Back())
                PrintOptions();
            else
            {
                lt.Reset();
                new QueryBuilder().Show();
                this.Close();
            }
        }

        //Displays relevant help according to current step.
        private void HelpButton_Click(object sender, RoutedEventArgs e)
        {
            if (first)
            {
                HelpWindow helpWindow = new HelpWindow("Please select a query type: ", "for example: Select");
                helpWindow.Show();
                children.Add(helpWindow);
            }
            else
            {
                HelpWindow helpWindow = new HelpWindow(lt.GetHelp(), lt.GetExample());
                helpWindow.Show();
                children.Add(helpWindow);
            }
        }

        //Handles close operation
        private void onClose()
        {
            while (children.Count > 0)
            {
                children.ElementAt(0).Close();
                children.Remove(children.ElementAt(0));
            }
        }

        //Handles close operation
        private void QBWindow_Closed(object sender, EventArgs e)
        {
            while (children.Count > 0)
            {
                children.ElementAt(0).Close();
                children.Remove(children.ElementAt(0));
            }
        }
    }
}
