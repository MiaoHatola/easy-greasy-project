﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UserInterface.Windows.Wizard
{
    /// <summary>
    /// Display information about the requested window.
    /// </summary>
    public partial class HelpWindow : MahApps.Metro.Controls.MetroWindow
    {
        public HelpWindow(string help, string example)
        {
            InitializeComponent();
            Help_Text.Text = help;
            Example_Text.Text = example;
        }
    }
}
