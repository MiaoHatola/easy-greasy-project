﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UserInterface.Windows.Wizard
{
    /// <summary>
    /// This window serves as an input method to the purchase window. it requests the user to choose an amount and returns
    /// it to the main window who called it.
    /// </summary>
    public partial class NumericUpDownDialog : MahApps.Metro.Controls.MetroWindow
    {
        private int sup, inf;
        private int numValue;
        private ShoppingCart sc;

        public int NumValueProperty
        {
            get { return numValue; }
            set
            {
                if (value <= sup && value >= inf)
                {
                    numValue = value;
                    txtNum.Text = value.ToString();
                }
                else
                    txtNum.Text = numValue.ToString();
            }
        }
        public NumericUpDownDialog(int sup, int inf, string text,ShoppingCart sc)
        {
            InitializeComponent();
            this.sc=sc;
            TextDisplay.Text = text;
            this.sup = sup;
            NumValueProperty = this.inf = inf;
            txtNum.Text = inf.ToString();
        }

        //Handles the slider up click
        private void cmdUp_Click(object sender, RoutedEventArgs e)
        {
            NumValueProperty++;
        }

        //Handles the slider down click
        private void cmdDown_Click(object sender, RoutedEventArgs e)
        {
            NumValueProperty--;
        }

        //Handles the slider value change
        private void txtNum_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtNum == null)
            {
                return;
            }
            int tmp;
            if (int.TryParse(txtNum.Text, out tmp))
                NumValueProperty = tmp;
        }

        //returns the value to the purchase window
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.sc.GetWantedQuantity(int.Parse(txtNum.Text));
            }
            catch
            {
                this.sc.GetWantedQuantity(inf);
            }
            finally
            {
                this.Close();
            }
        }
    }
}
