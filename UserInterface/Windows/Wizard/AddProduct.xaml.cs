﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Beckend.Entity;
using Beckend.Entity.BasicEntities;
using Beckend.DynamicQuerying.Queries;
using Beckend.DynamicQuerying.Conditions;

namespace UserInterface.Windows.Wizard
{
    /// <summary>
    /// A window for adding a new product to the database with ease.
    /// </summary>
    public partial class AddProductWindow : MahApps.Metro.Controls.MetroWindow
    {
        private string name;
        private int price, invID, location, stockCount;
        private ProductType type;

        private string[] types;

        public AddProductWindow()
        {
            InitializeComponent();
            types = Enum.GetNames(typeof(ProductType));
            for (int i = 0; i < types.Length; i++)
            {
                Box_Type.Items.Add(types[i]);    
            }
            Box_Type.SelectedIndex = 0;

            name = "";
            price = invID = location = stockCount = 0;
            type = (ProductType)Enum.Parse(typeof(ProductType), types[0]);

            SelectQuery sq = new SelectQuery();
            sq.SetDataName("Department");
            sq.SetWhere(new AllCondition());

            new DisplayObjects(Globals.LogicInstance.RunQuery(sq)).Show();
        }

        //Validates input and, if valid, saves the product to the database.
        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            string error_log = "";

            name = Box_Name.Text;
            int.TryParse(Box_Price.Text, out price);
            int.TryParse(Box_InvID.Text, out invID);
            int.TryParse(Box_Location.Text, out location);
            int.TryParse(Box_StockCount.Text, out stockCount);
            Enum.TryParse(Box_Type.Text, out type);

            if (name.Length < 3)
                error_log += "Bad product name\n";
            if (stockCount <= 0)
                error_log += "Stock count should be positive\n";
            if (price <= 0)
                error_log += "Price should be positive\n";
            if (location < 0)
                error_log += "Location shouldn't be negative\n";
            if (invID == 0)
                error_log += "Inventory ID shouldn't be 0\n";

            if (!error_log.Equals(""))
                MessageBox.Show(error_log);
            else
            {
                Product obj = null;
                InsertQuery iq = new InsertQuery();
                SelectQuery sq = new SelectQuery();
                iq.SetDataName("Product");
                sq.SetDataName("Product");

                iq.SetInsert(new Product(name, price, type, invID, location, stockCount,0));
                object[] results = Globals.LogicInstance.RunQuery(iq);
                if (results != null && results.Length > 0)
                    obj = results[0] as Product;

                if (obj != null)
                {
                    MessageBox.Show("Successfuly Created Product!");
                    sq.SetWhere(new SingleCondition(EntityHelper.GetPropertyByName(typeof(Product), "ID"), 0, obj.ID.ToString()));
                    new DisplayObjects(false, obj).Show();
                }
                else
                    MessageBox.Show("Sorry, your product couldn't be saved to the database. Please try again later.");

                this.Close();
            }
        }
    }
}
