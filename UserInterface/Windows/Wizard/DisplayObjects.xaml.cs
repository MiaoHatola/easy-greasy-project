﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Reflection;
using Beckend.Entity;
using Beckend.Entity.BasicEntities;
using Beckend.DynamicQuerying.Queries;
using Beckend.DynamicQuerying.Conditions;
using Logic;

namespace UserInterface.Windows
{
    /// <summary>
    /// A blank datagrid that is capable of displaying an array of objects sent to it.
    /// </summary>
    public partial class DisplayObjects : MahApps.Metro.Controls.MetroWindow
    {
        private object[] originalObjects;
        private bool editable;
        private SelectQuery filter;
        private PropertyInfo[] pi;

        public DisplayObjects(object[] objects) : this(false, objects)
        { }
        public DisplayObjects(bool editable, params object[] objects)
        {
            InitializeComponent();
            if (objects == null)
                return;
            this.editable = editable;
            this.originalObjects = objects;

            filter = new SelectQuery();
            filter.SetDataName("DataGrid");
            if (objects.Length > 0)
                pi = EntityHelper.GetReadableProperties(((DynamicEntity)originalObjects[0]).GetEntityType());

            if (editable)
            {
                SaveButton.Visibility = Visibility.Visible;
                DisplayGrid.IsReadOnly = false;
                DisplayGrid.CanUserSortColumns = true;
                DisplayGrid.CanUserAddRows = true;
            }
            else
            {
                SaveButton.Visibility = Visibility.Hidden;
                DisplayGrid.IsReadOnly = true;
                DisplayGrid.CanUserSortColumns = false;
                DisplayGrid.CanUserAddRows = false;
            }
            DisplayGrid.ItemsSource = new ObservableCollection<object>(this.originalObjects);
        }

        //Saves the changes made to the grid
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            LinkedList<DynamicEntity> toDelete = new LinkedList<DynamicEntity>();
            LinkedList<DynamicEntity> toSave = new LinkedList<DynamicEntity>();

            for (int i = 0; i < originalObjects.Length; i++)
            {
                if (!DisplayGrid.Items.Contains(originalObjects[i]))
                    toDelete.AddFirst((DynamicEntity)originalObjects[i]);
                else
                    toSave.AddFirst((DynamicEntity)originalObjects[i]);
            }
            if (toDelete.Count > 0)
                Globals.LogicInstance.DeleteObjects(EntityHelper.GetNameFromType(((DynamicEntity)toDelete.ElementAt(0)).GetEntityType()), toDelete.ToArray());
            if (toSave.Count > 0)
                Globals.LogicInstance.Save(EntityHelper.GetNameFromType((originalObjects[0] as DynamicEntity).GetEntityType()), this.originalObjects = toSave.ToArray());
        }

        //Handles edits so that the grid refreshes relevant values in real time (if stockcount is 0 then isInStock turns to False).
        private void DisplayGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key.Equals(Key.Enter))
            {
                DisplayGrid.Items.Refresh();
            }
        }

        //handles the specific case when the objects to display are customers or clubmembers (Transaction display)
        private void dgTransClubMember_Loaded(object sender, RoutedEventArgs e)
        {
            DataGrid listView = sender as DataGrid;
            if (!(originalObjects[0] is IBuyer))
            {
                listView.Visibility = Visibility.Hidden;
                listView.Height = 0;
            }
            if((DisplayGrid.SelectedItem != null) && DisplayGrid.SelectedItem is IBuyer)
            {
                //listView.ItemsSource = new ObservableCollection<Transaction>(((IBuyer)DisplayGrid.SelectedItem).GetTransaction());
                listView.IsReadOnly = !editable;
                listView.Items.Refresh();
            }
        }

        private void filterInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            ICondition c;
            try
            {
                string input = removeDoubleSpaces(cutEdges(filterInput.Text));
                if (input.ToUpper() == "ALL")
                    c = new AllCondition();
                else if (input.ToLower() == "NONE")
                    c = new NoneCondition();
                else
                    c = new OrCondition(input, pi);
                
                filter.SetWhere(c);
                filter.SetQuerable(castToDynamicEntityList(originalObjects));
                filterInfo.Text = filter.ToString();
                DisplayGrid.ItemsSource = filter.RunQuery();
            }
            catch
            {
                DisplayGrid.ItemsSource = originalObjects;
                filterInfo.Text = "Incorrect input.";
            }
        }
        private string cutEdges(string str)
        {
            int i = 0;
            while (str[i] == ' ')
                i++;
            int j = str.Length;
            while (str[j - 1] == ' ')
                j--;
            return str.Substring(i, j - i);
        }
        private string removeDoubleSpaces(string str)
        {
            string ans = "";
            if (str.Length > 0)
                ans += str[0];
            for (int i = 1; i < str.Length; i++)
                if (str[i] != ' ' || str[i - 1] != ' ')
                    ans += str[i];
            return ans;
        }
        private List<DynamicEntity> castToDynamicEntityList(object[] arr)
        {
            List<DynamicEntity> lst = new List<DynamicEntity>();
            for (int i = 0; i < arr.Length; i++)
                lst.Add((DynamicEntity)arr[i]);
            return lst;
        }
    }
}
