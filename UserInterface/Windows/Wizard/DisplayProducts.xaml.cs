﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Beckend.Entity;
using Beckend.Entity.BasicEntities;
using Beckend.DynamicQuerying.Queries;
using Beckend.DynamicQuerying.Conditions;

namespace UserInterface.Windows.Wizard
{
    /// <summary>
    /// A window to display all products in the database. useful instead of running the same query every window.
    /// </summary>
    public partial class DisplayProducts : MahApps.Metro.Controls.MetroWindow
    {
        private Product[] prods;
        private Department[] deps;
        ObservableCollection<Product> src;

        public DisplayProducts()
        {
            InitializeComponent();
            DepartmentsCombo.Items.Add("All");

            // initialize products
            InitializeProducts();
            // initialize departments
            object[] tmp = Globals.LogicInstance.GetAllData("Department");
            deps = new Department[tmp.Length];
            for (int i = 0; i < tmp.Length; i++)
            {
                deps[i] = tmp[i] as Department;
                DepartmentsCombo.Items.Add(deps[i].DepartmentName);
            }
            DepartmentsCombo.SelectedIndex = 0;

            ProductsDataGrid.CanUserAddRows = true;
            ProductsDataGrid.CanUserDeleteRows = true;
            ProductsDataGrid.IsReadOnly = false;
        }

        //prepares the products for display.
        private void InitializeProducts()
        {
            object[] tmp = Globals.LogicInstance.GetAllData("Product");
            prods = new Product[tmp.Length];
            for (int i = 0; i < tmp.Length; i++)
                prods[i] = tmp[i] as Product;
        }

        //Department filter handler.
        private void DepartmentsCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectQuery sq = new SelectQuery();
            sq.SetQuerable(prods.ToList<DynamicEntity>());
            ICondition cond;

            ComboBox depa = (ComboBox)sender;
            
            if (((string)depa.SelectedValue).Equals("All"))
                cond = new AllCondition();
            else
            {
                string depatmentName = (string)depa.SelectedValue;
                int departmentID = -1;
                foreach (Department dep in deps)
                {
                    if (dep.DepartmentName.Equals(depatmentName))
                    {
                        departmentID = dep.DepartmentID;
                        break;
                    }
                }
                if (departmentID != -1)
                    cond = new SingleCondition(EntityHelper.GetPropertyByName(typeof(Product), "Location"), 0, departmentID.ToString());
                else
                    cond = new NoneCondition();
            }
            sq.SetWhere(cond);

            ProductsDataGrid.ItemsSource = src = new ObservableCollection<Product>(castToProducts(sq.RunQuery()));
        }

        //casts all objects in data to Products
        private Product[] castToProducts(object[] arr)
        {
            Product[] ans = new Product[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                ans[i] = arr[i] as Product;
            }
            return ans;
        }

        //Saves changes (if editing is enabled)
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            LinkedList<Product> toSave = new LinkedList<Product>();
            LinkedList<Product> toDelete = new LinkedList<Product>();
            for (int i = 0; i < prods.Length; i++)
            {
                if (!ProductsDataGrid.Items.Contains(prods[i]))
                    toDelete.AddFirst(prods[i]);
                else
                    toSave.AddFirst(prods[i]);
            }
            if (toDelete.Count > 0)
                Globals.LogicInstance.DeleteObjects("Product", toDelete.ToArray());

            Globals.LogicInstance.Save("Product", toSave.ToArray());
        }
    }
}
