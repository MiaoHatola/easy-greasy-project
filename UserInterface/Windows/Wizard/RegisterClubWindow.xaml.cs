﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Beckend.Entity.BasicEntities;
using Beckend.DynamicQuerying.Queries;
using Beckend.DynamicQuerying.Conditions;
using Beckend.Entity;

namespace UserInterface.Windows.Wizard
{
    /// <summary>
    /// Enables registering of a new club member from a previously existing customer.
    /// </summary>
    public partial class RegisterClubWindow : MahApps.Metro.Controls.MetroWindow
    {
        private int tz;
        private Gender gender;

        public string FNAME { get; set; }
        public string LNAME { get; set; }
        public string TZ 
        { 
            get { return this.tz.ToString(); } 
            set 
            {
                try { this.tz = int.Parse(value); }
                catch { this.tz = 0; } 
            } 
        }
        public DateTime BIRTHDAY { get; set; }
        public string GENDER 
        {
            get { return gender.ToString(); }
            set { this.gender = (value == "Male") ? Gender.Male : Gender.Female; }
        }

        public RegisterClubWindow()
        {
            InitializeComponent();

            UserDatePick.SelectedDate = DateTime.Now;
        }

        //Validates input and, if valid, saves the new clubmember to the database.
        private void SignUpButton_Click(object sender, RoutedEventArgs e)
        {
            if (UserDatePick.SelectedDate == null)
            {
                MessageBox.Show("Please choose a proper date.");
                return;
            }

            FNAME = FNBox.Text;
            LNAME = LNBox.Text;
            TZ = TZBox.Text;
            GENDER = GenderBox.Text;
            BIRTHDAY = UserDatePick.SelectedDate.Value;

            if (FNAME.Length < 3 || LNAME.Length < 3)
                MessageBox.Show("Your name should be at least 3 characters long.");
            else if (tz == 0 || TZ.Length < 9)
                MessageBox.Show("Your teudat zehut is in the wrong format.\nplease notice that we only accept Israeli ID.");
            else
            {
                BIRTHDAY = UserDatePick.SelectedDate.Value;
                FNAME = FNBox.Text;
                LNAME = LNBox.Text;
                GENDER = GenderBox.Text;

                User user = Globals.CurrentUser;
                ClubMember c = new ClubMember(user.Username, user.Password, "Customer", FNAME, LNAME, tz, BIRTHDAY.Date, gender,"");

                // Delete current user
                Globals.LogicInstance.DeleteObjects("Customer", user);
                
                // Insert new ClubMember
                InsertQuery iq = new InsertQuery();
                iq.SetDataName("ClubMember");
                iq.SetInsert(c);
                object[] result;
                if ((result = Globals.LogicInstance.RunQuery(iq)) != null)
                {
                    Globals.CurrentUser = (ClubMember)result[0];

                    EditQuery changeTransaction = new EditQuery();
                    changeTransaction.SetDataName("Transaction");
                    changeTransaction.SetEdit(EntityHelper.GetPropertyByName(typeof(Transaction),"CustomerID"),Globals.CurrentUser.ID.ToString());
                    Globals.LogicInstance.RunQuery(changeTransaction);
                    MessageBox.Show("Thank you for registering to our club!");
                }
                else
                    MessageBox.Show("Sorry, an error occured.");
                Globals.OpenCurrentProfile();
                this.Close();
            }
        }
    }
}
