﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UserInterface.Windows;
using Beckend.Entity.BasicEntities;

namespace UserInterface.Windows
{
    /// <summary>
    /// The login window. this window is the first window that the user encounters. it enables logging in to the system,
    /// or registering as a new user.
    /// </summary>
    public partial class LoginWindow : MahApps.Metro.Controls.MetroWindow
    {
        public LoginWindow()
        {
            InitializeComponent();
            username_txt.Focus();
        }
        //Initiates loginSequence() which is the method responsible for checking the user's credentials and checking the user clearance.
        private void Login_Click(object sender, RoutedEventArgs e)
        {
            if (username_txt.Text == "" || passsword_txt.Password == "")
                return;
            User user = Globals.LogicInstance.LoginSequence(username_txt.Text, passsword_txt.Password);
            if (user != null)
            {
                Globals.CurrentUser = user;
                Globals.OpenCurrentProfile();
                this.Close();
            }
            else
            {
                MessageBox.Show("Wrong Credentials!");
            }
        }
        //Loads the "Register" window, for new customers to sign up.
        private void Register_Click(object sender, RoutedEventArgs e)
        {
            Window register = new RegisterWindow();
            register.Show();
        }
    }
}
