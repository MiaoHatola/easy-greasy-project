﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Logic;
using Beckend.DynamicQuerying.Queries;
using Beckend.DynamicQuerying.Conditions;
using Beckend.Entity.BasicEntities;
using Beckend.Entity;

namespace UserInterface.Windows
{
    /// <summary>
    /// This window enables any user to edit his own information. it restricts access to fields that are read only.
    /// </summary>
    public partial class ChangeInfoWin : MahApps.Metro.Controls.MetroWindow
    {
        public ChangeInfoWin()
        {
            InitializeComponent();
            UsernameBox.Text = Globals.CurrentUser.Username;
            PasswordBox.Password = ConfirmPasswordBox.Password = Globals.CurrentUser.Password;
        }

        //Save the new information.
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (UsernameBox.Text == Globals.CurrentUser.Username && PasswordBox.Password == Globals.CurrentUser.Password)
            {
                MessageBox.Show("Nothing to change.");
                return;
            }
            bool usernameCheck = true;
            if (UsernameBox.Text != Globals.CurrentUser.Username)
            {
                SelectQuery sq = new SelectQuery();
                sq.SetWhere(new SingleCondition(EntityHelper.GetPropertyByName(typeof(User), "Username"), 0, UsernameBox.Text));
                usernameCheck = Globals.LogicInstance.RunQuery(sq) == null;
            }
            if (!usernameCheck)
                MessageBox.Show("Username is taken. Please choose another.");
            else if (PasswordBox.Password != ConfirmPasswordBox.Password)
                MessageBox.Show("Password confirmation failed.");
            else
            {
                bool un = UsernameBox.Text != Globals.CurrentUser.Username;
                bool pass = PasswordBox.Password != Globals.CurrentUser.Password;
                string changed = "";
                if (un)
                    changed += " username";
                if (un && pass)
                    changed += " and";
                if (pass)
                    changed += " password";
                Globals.CurrentUser.Username = UsernameBox.Text;
                Globals.CurrentUser.Password = PasswordBox.Password;
                if (Globals.LogicInstance.Save(EntityHelper.GetNameFromType(Globals.CurrentUser.GetEntityType()), Globals.CurrentUser))
                    MessageBox.Show("Successfuly changed" + changed + ".");
                else
                    MessageBox.Show("Failed to change " + changed + ".");

                this.Close();
            }
        }

        //Handle closing operation
        private void ChangeInfoWin_Closing(object sender, EventArgs e)
        {
            Globals.OpenCurrentProfile();
        }
    }
}
