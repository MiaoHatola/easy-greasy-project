﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UserInterface.Windows
{
    /// <summary>
    /// Interaction logic for DebugWindow.xaml
    /// </summary>
    public partial class DebugWindow : Window
    {
        public DebugWindow()
        {
            InitializeComponent();
            this.Input.Focus();
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            
            if (Key.Escape.Equals((Key)e.Key))
                ((TextBox)sender).Text = "";
            if (Key.Enter.Equals((Key)e.Key))
            {
                if (((TextBox)sender).Text.ToUpper().Equals("EXIT"))
                    Environment.Exit(0);
                try
                {
                    Globals.LogicInstance.RunSQL(this.Input.Text);
                }
                catch(Exception ex)
                {
                    ((TextBox)sender).Text = ex.Message;
                }
            }
        }

        private void Input_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.Input.Text = "";
        }

    }
}
