﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UserInterface.Windows.Wizard;
using Beckend.DynamicQuerying.Queries;
using Beckend.DynamicQuerying.Conditions;
using Beckend.Entity;
using Beckend.Entity.BasicEntities;

namespace UserInterface.Windows
{
    /// <summary>
    /// This is the admin window. it contains all privlliges, and enables the admin to acces every piece of information on the system.
    /// </summary>
    public partial class AdminWindow : MahApps.Metro.Controls.MetroWindow
    {
        List<Window> children = new List<Window>();
        private User admin;
        public AdminWindow()
        {
            InitializeComponent();
            admin = Globals.CurrentUser;
            InfoBlock.Text = "Username:\t";
            InfoBlock.Text += admin.Username + "\n";
            InfoBlock.Text += "User Type:\t";
            InfoBlock.Text += admin.Type + "\n";
            InfoBlock.Text += "ID:\t\t";
            InfoBlock.Text += admin.ID + "\n";

            if (Globals.CurrentUser is Employee)
            {
                Employee p = Globals.CurrentUser as Employee;

                TextBlock1.Text += "Name:\n";
                TextBlock1.Text += "Gender:\n";
                TextBlock1.Text += "Teudat Zehut:\n";
                TextBlock1.Text += "Department ID:\n";
                TextBlock1.Text += "Salary:\n";
                TextBlock1.Text += "Supervisor ID:\n";

                TextBlock2.Text += p.FirstName + " " + p.LastName + "\n";
                TextBlock2.Text += p.Gender + "\n";
                TextBlock2.Text += p.TeudatZehute + "\n";
                TextBlock2.Text += p.DepartmentID + "\n";
                TextBlock2.Text += p.Salary + "\n";
                TextBlock2.Text += p.SupervisorID + "\n";
            }
        }

        //Access to the query builder for full control over the database.
        private void RunQuery_Click(object sender, RoutedEventArgs e)
        {
            QueryBuilder window = new QueryBuilder();
            children.Add(window);
            window.Show();
        }

        //change the admin's personal info
        private void ChangeInfoButton_Click(object sender, RoutedEventArgs e)
        {
            ChangeInfoWin ci = new ChangeInfoWin();
            ci.Show();
            onClose();
            this.Close();
        }
        
        //Views all of the recent queries that were used on the database, for easy troubleshooting errors or file corruption.
        private void MyHistory_Click(object sender, RoutedEventArgs e)
        {
            DisplayObjects window = new DisplayObjects(false, Globals.GetQueryHistory().ToArray());
            children.Add(window);
            window.Show();
        }

        //logs out of the admin user.
        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            onClose();
            Globals.LogOut();
        }

        //displays all the products on the system.
        private void Products_Click(object sender, RoutedEventArgs e)
        {
            DisplayProducts t = new DisplayProducts();
            t.Show();
            children.Add(t);

        }

        //Displays all customers
        private void CustomersButton_Click(object sender, RoutedEventArgs e)
        {
            List<object> tmp = Globals.LogicInstance.GetAllData("Customer").ToList();
            tmp.AddRange(Globals.LogicInstance.GetAllData("ClubMember"));

            DisplayObjects t = new DisplayObjects(true, tmp.ToArray());
            t.Show();
            children.Add(t);
        }

        //Displays all club members
        private void ClubMembers_Click(object sender, RoutedEventArgs e)
        {
            List<object> tmp = Globals.LogicInstance.GetAllData("ClubMember").ToList();
            DisplayObjects t = new DisplayObjects(true, tmp.ToArray());
            t.Show();
            children.Add(t);
        }

        //Handles the closing operation
        private void onClose()
        {
            while (children.Count > 0)
            {
                children.ElementAt(0).Close();
                children.Remove(children.ElementAt(0));
            }
        }

        //Handles the close operation
        private void AdminWin_Closed(object sender, EventArgs e)
        {
            while (children.Count > 0)
            {
                children.ElementAt(0).Close();
                children.Remove(children.ElementAt(0));
            }
        }

        //Creates a new product in the system with ease.
        private void NewProductButton_Click(object sender, RoutedEventArgs e)
        {
            AddProductWindow window = new AddProductWindow();
            children.Add(window);
            window.Show();
        }
    }
}
