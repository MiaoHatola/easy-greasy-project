﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Beckend.Entity;
using Beckend.Entity.BasicEntities;
using UserInterface.Windows.Wizard;
using Beckend.DynamicQuerying.Queries;
using Beckend.DynamicQuerying.Conditions;

namespace UserInterface.Windows
{
    /// <summary>
    /// This is the customer window that enables a customer to change his password, view his info, become a clubmember or purchase.
    /// </summary>
    public partial class CustomerWindow : MahApps.Metro.Controls.MetroWindow
    {
        List<Window> children = new List<Window>();
        public CustomerWindow()
        {
            InitializeComponent();
            string[] info = Globals.GetCurrentUserInfo();
            InfoTextBlock1.Text = info[0];
            InfoTextBlock2.Text = info[1];
            InfoTextBlock1.Text += "\nClubMember:";
            InfoTextBlock2.Text += "\n" + (Globals.CurrentUser is ClubMember);

            if (Globals.CurrentUser is ClubMember)
            {
                ClubMember p = Globals.CurrentUser as ClubMember;

                RegisterClubButton.Visibility = Visibility.Hidden;

                InfoTextBlock1.Text += "\n\n";
                InfoTextBlock1.Text += "Name:\n";
                InfoTextBlock1.Text += "Gender:\n";
                InfoTextBlock1.Text += "Date Of Birth: \n";
                InfoTextBlock1.Text += "Teudat Zehut:\n";

                InfoTextBlock2.Text += "\n\n";
                InfoTextBlock2.Text += p.FirstName + " " + p.LastName + "\n";
                InfoTextBlock2.Text += p.Gender + "\n";
                InfoTextBlock2.Text += p.DateOfBirth.Day.ToString() + "." + p.DateOfBirth.Month.ToString() + "." + p.DateOfBirth.Year.ToString() + "\n";
                InfoTextBlock2.Text += p.TeudatZehute;
            }
        }
        //Change the current user info window.
        private void ChangeInfoButton_Click(object sender, RoutedEventArgs e)
        {
            ChangeInfoWin window = new ChangeInfoWin();
            window.Show();
            onClose();
            this.Close();
        }
        //Opens the Purchase window for the customer.
        private void StartShopping_Button_Click(object sender, RoutedEventArgs e)
        {
            ShoppingCart window = new ShoppingCart();
            if (window.hasProducts())
            {
                window.Show();
                children.Add(window);
            }
            else
            {
                window.Close();
                MyPopUp pop = new MyPopUp("No products in stock right now! Sorry!");
                pop.Show();
            }
        }
        //Register the current customer as a Clubmember
        private void RegisterClubButton_Click(object sender, RoutedEventArgs e)
        {
            new RegisterClubWindow().Show();
            onClose();
            this.Close();
        }
        //Logout of the current user and load the login page.
        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            onClose();
            Globals.LogOut();
        }

        //handles the close operatin (close all child windows)
        private void onClose()
        {
            while (children.Count > 0)
            {
                children.ElementAt(0).Close();
                children.Remove(children.ElementAt(0));
            }
        }
        //handles the close operatin (close all child windows)
        private void Window_Closed(object sender, EventArgs e)
        {
            while (children.Count > 0)
            {
                children.ElementAt(0).Close();
                children.Remove(children.ElementAt(0));
            }
        }
        //views the customers past transactions
        private void ViewTransactionsButton_Click(object sender, RoutedEventArgs e)
        {
            SelectQuery sq = new SelectQuery();
            sq.SetDataName("Transaction");
            sq.SetWhere(new SingleCondition("CustomerID = " + Globals.CurrentUser.ID.ToString(), EntityHelper.GetReadableProperties(typeof(Transaction))));
            int x = Globals.CurrentUser.ID;

            Window w = new DisplayObjects(false, Globals.LogicInstance.RunQuery(sq));
            children.Add(w);
            w.Show();
        }

        private void FindShopButton_Click(object sender, RoutedEventArgs e)
        {
            new ShopBrowser().Show();
            onClose();
            this.Close();
        }
    }
}
