﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UserInterface.Windows.Wizard;

namespace UserInterface.Windows
{
    /// <summary>
    /// Interaction logic for ShopBrowser.xaml
    /// </summary>
    public partial class ShopBrowser : MahApps.Metro.Controls.MetroWindow
    {
        public ShopBrowser()
        {
            InitializeComponent();
        }

        private void Faggot_Click(object sender, RoutedEventArgs e)
        {
            new ShopProfile("Tel Aviv").Show();
        }

        private void Paradise_Click(object sender, RoutedEventArgs e)
        {
            new ShopProfile("Beer Sheva").Show();
        }

        private void Nameless_Click(object sender, RoutedEventArgs e)
        {
            new ShopProfile("Lehavim").Show();
        }

        private void Uranium_Click(object sender, RoutedEventArgs e)
        {
            new ShopProfile("Afula").Show();
        }

        private void Butthole_Click(object sender, RoutedEventArgs e)
        {
            new ShopProfile("Eilat").Show();
        }

        private void ShopBrowserWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Globals.OpenCurrentProfile();
        }

        private void Naha_Click(object sender, RoutedEventArgs e)
        {
            new ShopProfile("Naharia").Show();
        }

        private void Tibaerias_Click(object sender, RoutedEventArgs e)
        {
            new ShopProfile("Tiberias").Show();
        }

        private void Tzfat_Click(object sender, RoutedEventArgs e)
        {
            new ShopProfile("Tzfat").Show();
        }

        private void Haifa_Click(object sender, RoutedEventArgs e)
        {
            new ShopProfile("Haifa").Show();
        }

        private void Lod_Click(object sender, RoutedEventArgs e)
        {
            new ShopProfile("Lod").Show();
        }
    }
}