﻿using Beckend.Entity;
using Beckend.Entity.BasicEntities;
using UserInterface.Windows.Wizard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UserInterface.Windows
{
    /// <summary>
    /// The worker window contains all the neccesary information to the worker, and enables a worker to access all of his privlliges.
    /// </summary>
    public partial class WorkerWindow : MahApps.Metro.Controls.MetroWindow
    {
        List<Window> children = new List<Window>();
        public WorkerWindow()
        {
            InitializeComponent();
            string[] content = Globals.GetCurrentUserInfo();
            TextBlock1.Text = content[0];
            TextBlock2.Text = content[1];
            if (Globals.CurrentUser is Employee)
            {
                Employee p = Globals.CurrentUser as Employee;

                TextBlock1.Text += "\n";
                TextBlock1.Text += EntityHelper.GetNameFromType(p.GetEntityType()) + "Details: \n"; ;
                TextBlock1.Text += "Name:\n";
                TextBlock1.Text += "Gender:\n";
                TextBlock1.Text += "Teudat Zehut:\n";
                TextBlock1.Text += "Department I.D.:\n";
                TextBlock1.Text += "Salary:\n";
                TextBlock1.Text += "Supervisor I.D.:\n";

                TextBlock2.Text += "\n\n";
                TextBlock2.Text += p.FirstName + " " + p.LastName + "\n";
                TextBlock2.Text += p.Gender + "\n";
                TextBlock2.Text += p.TeudatZehute + "\n";
                TextBlock2.Text += p.DepartmentID + "\n";
                TextBlock2.Text += p.Salary + "\n";
                TextBlock2.Text += p.SupervisorID + "\n";
            }
        }
        /// <summary>
        /// The next section provides the new windows that are available to the worker.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeInfoButton_Click(object sender, RoutedEventArgs e)
        {
            Window chngInfo = new ChangeInfoWin();
            chngInfo.Show();
            onClose();
            this.Close();
        }

        private void Customers_Click(object sender, RoutedEventArgs e)
        {
            List<object> tmp = Globals.LogicInstance.GetAllData("Customer").ToList();
            tmp.AddRange(Globals.LogicInstance.GetAllData("ClubMember"));

            DisplayObjects t = new DisplayObjects(false, tmp.ToArray());
            children.Add(t);
            t.Show();
        }

        private void Products_Click(object sender, RoutedEventArgs e)
        {
            DisplayProducts display = new DisplayProducts();
            display.Show();
            children.Add(display);
        }

        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            onClose();
            Globals.LogOut();
        }
        private void ClubMembers_Click(object sender, RoutedEventArgs e)
        {
            List<object> tmp = Globals.LogicInstance.GetAllData("ClubMember").ToList();
            DisplayObjects t = new DisplayObjects(false, tmp.ToArray());
            t.Show();
            children.Add(t);
        }
        private void onClose()
        {
            while (children.Count > 0)
            {
                children.ElementAt(0).Close();
                children.Remove(children.ElementAt(0));
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            while (children.Count > 0)
            {
                children.ElementAt(0).Close();
                children.Remove(children.ElementAt(0));
            }
        }
        
    }
}
