﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Beckend.DynamicQuerying.Queries;
using Beckend.DynamicQuerying.Conditions;
using Beckend.Entity.BasicEntities;
using Beckend.Entity;

namespace UserInterface.Windows
{
    /// <summary>
    /// The window that registers new users to the system.
    /// </summary>
    public partial class RegisterWindow : MahApps.Metro.Controls.MetroWindow
    {
        private SelectQuery sq;

        public RegisterWindow()
        {
            InitializeComponent();
            sq = new SelectQuery();
            this.Username.Focus();
        }
        /// <summary>
        /// Methods that supply hints to the new user about the fields in the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Username_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (((TextBox)sender).Text.Equals(((TextBox)sender).Name))
                ((TextBox)sender).Text = "";
        }
        private void Username_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (((TextBox)sender).Text.Equals(""))
                ((TextBox)sender).Text = ((TextBox)sender).Name;
        }

        /// <summary>
        /// The "Register" button. This event handler checks the input and saves the new user to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            string error_log = "";

            if (Username.Text.Length < 3 || Username.Text.ToUpper() == "USERNAME")
                error_log += "Please choose a proper username\n";
            else
            {
                sq.SetWhere(new SingleCondition(EntityHelper.GetPropertyByName(typeof(User), "Username"), 0, Username.Text));
                string inUse = "Username is already in use, please choose another\n";

                sq.SetDataName("Customer");
                if (Globals.LogicInstance.RunQuery(sq).Length > 0)
                {
                    error_log += inUse;
                }
                else
                {
                    sq.SetDataName("ClubMember");
                    if (Globals.LogicInstance.RunQuery(sq).Length > 0)
                        error_log += inUse;
                    else
                    {
                        sq.SetDataName("Employee");
                        if (Globals.LogicInstance.RunQuery(sq).Length > 0)
                            error_log += inUse;
                    }
                }
            }
            if (PassBox.Password == "")
                error_log += "Please insert a password\n";
            if (PassBox.Password != PassConfBox.Password)
                error_log += "Password confirmation failed\n";

            if (error_log == "")
            {
                InsertQuery iq = new InsertQuery();
                iq.SetDataName("Customer");
                iq.SetInsert(new Customer(Username.Text, PassConfBox.Password, "Customer", ""));
                if (Globals.LogicInstance.RunQuery(iq) != null)
                    MessageBox.Show("Registered Successfuly!");
                else
                    MessageBox.Show("Sorry, an error occured.");

                this.Close();
            }
            else
                MessageBox.Show(error_log);
        }
    }
}
