﻿using Beckend.DynamicQuerying.Conditions;
using Beckend.DynamicQuerying.Queries;
using Beckend.Entity;
using Beckend.Entity.BasicEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UserInterface.Windows.Wizard;

namespace UserInterface.Windows
{
    /// <summary>
    /// This is the Manager window. it enables a manager to access all of his privilliges.
    /// </summary>
    public partial class ManagerWindow : MahApps.Metro.Controls.MetroWindow
    {
        List<Window> children = new List<Window>();
        public ManagerWindow()
        {
            InitializeComponent();
            string[] content = Globals.GetCurrentUserInfo();
            TextBlock1.Text = content[0];
            TextBlock2.Text = content[1];
            if (Globals.CurrentUser is Employee)
            {
                Employee p = Globals.CurrentUser as Employee;

                TextBlock1.Text += "\n";
                TextBlock1.Text += EntityHelper.GetNameFromType(p.GetEntityType()) + "Details: \n"; ;
                TextBlock1.Text += "Name:\n";
                TextBlock1.Text += "Gender:\n";
                TextBlock1.Text += "Teudat Zehut:\n";
                TextBlock1.Text += "Department ID:\n";
                TextBlock1.Text += "Salary:\n";
                TextBlock1.Text += "Supervisor ID:\n";

                TextBlock2.Text += "\n\n";
                TextBlock2.Text += p.FirstName + " " + p.LastName + "\n";
                TextBlock2.Text += p.Gender + "\n";
                TextBlock2.Text += p.TeudatZehute + "\n";
                TextBlock2.Text += p.DepartmentID + "\n";
                TextBlock2.Text += p.Salary + "\n";
                TextBlock2.Text += p.SupervisorID + "\n";
            }
        }

        // This method opens a "Change info" window for the manager to edit his personal info (mainly password)
        private void ChangeInfoButton_Click(object sender, RoutedEventArgs e)
        {
            Window chngInfo = new ChangeInfoWin();
            chngInfo.Show();
            onClose();
            this.Close();
        }
        //Displays the customers
        private void Customers_Click(object sender, RoutedEventArgs e)
        {
            List<object> tmp = Globals.LogicInstance.GetAllData("Customer").ToList();
            tmp.AddRange(Globals.LogicInstance.GetAllData("ClubMember"));

            DisplayObjects t = new DisplayObjects(true, tmp.ToArray());
            t.Show();
            children.Add(t);
        }
        //Displays the Products
        private void Products_Click(object sender, RoutedEventArgs e)
        {
            DisplayProducts display = new DisplayProducts();
            children.Add(display);
            display.Show();
        }
        //Displays the employees under this manager's supervision.
        private void GetEmployees_Click(object sender, RoutedEventArgs e)
        {
            SelectQuery employees = new SelectQuery();

            //set Properties to get all Customers]
            employees.SetWhere(new SingleCondition(EntityHelper.GetPropertyByName(typeof(Employee),"SupervisorID"),0,((Employee)Globals.CurrentUser).ID.ToString()));
            employees.SetDataName("Employee");
            DisplayObjects display = new DisplayObjects(true, Globals.LogicInstance.RunQuery(employees));
            children.Add(display);
            display.Show();
        }
        //Logouts of the manager account.
        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            onClose();
            Globals.LogOut();
        }
        //Displays all of the clubmembers
        private void ClubMembers_Click(object sender, RoutedEventArgs e)
        {
            List<object> tmp = Globals.LogicInstance.GetAllData("ClubMember").ToList();
            DisplayObjects t = new DisplayObjects(true, tmp.ToArray());
            t.Show();
            children.Add(t);
        }
        //Close operation to close all child windows.
        private void onClose()
        {
            while (children.Count > 0)
            {
                children.ElementAt(0).Close();
                children.Remove(children.ElementAt(0));
            }
        }
        //Handles the window close event
        private void ManagerWin_Closed(object sender, EventArgs e)
        {
            while (children.Count > 0)
            {
                children.ElementAt(0).Close();
                children.Remove(children.ElementAt(0));
            }
        }

        private void NewProductButton_Click(object sender, RoutedEventArgs e)
        {
            Window window = new AddProductWindow();
            children.Add(window);
            window.Show();
        }
    }
}
