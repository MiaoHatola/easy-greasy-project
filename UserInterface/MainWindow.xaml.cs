﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UserInterface.Windows;
using UserInterface.Windows.Wizard;

using Beckend.DynamicQuerying.Queries;
using Beckend.DynamicQuerying.Conditions;
using Beckend.Entity.BasicEntities;
using Beckend.Entity;

namespace UserInterface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.Hide();
            // initializing all the tiers recursively for the first time
            Globals.InitializeLogic();
            new LoginWindow().Show();
            this.Close();
        }

        private void MediaPlayer_Loaded(object sender, RoutedEventArgs e)
        {
            this.Show();
            MediaPlayer.Play();
        }

        private void MediaPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new LoginWindow().Show();
            this.Close();
        }
    }
}
