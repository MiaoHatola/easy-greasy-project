﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Handlers
{
    interface IDatabaseHandler
    {
        bool Save(string filename, object toSave);
        object Load(string filename);
    }
}
