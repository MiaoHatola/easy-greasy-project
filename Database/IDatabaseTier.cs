﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Beckend.DynamicQuerying;
using Beckend.Entity;

namespace Database
{
    public interface IDatabaseTier
    {
        /*
         * the database tier accepts a query and a dataname and provides it the relevant db object (a list or sql provider...)
         */
        bool SubmitQuery(Query query);
        /*
         * run the submitted query or return a relevant error message
         */
        object[] RunQuery();
        /*
         * save and exit
         */
        void RunSQL(string cmd);
        bool RequestStop();
        /*
         * forget the submitted query
         */
        void Flush();
        /*
         * add a query to the query history Database
         */
        bool Save(string dataName, params object[] obj);
        bool DeleteObjects(string dataName, object[] de);
    }
}
