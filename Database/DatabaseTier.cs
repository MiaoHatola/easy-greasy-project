﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Database.Handlers;
using Beckend.Entity;
using Beckend.Entity.BasicEntities;
using Beckend.DynamicQuerying;
using Beckend.DynamicQuerying.Queries;
using System.Data.Linq;
using System.Data.SqlClient;
using Beckend.DynamicQuerying.Conditions;
using System.Reflection;
namespace Database
{
    public class DatabaseTier : IDatabaseTier
    {
        public static string SubmitFailed = "Query Submit Failed.";
        public static string NullQuery = "No Query Is Set.";

        private string connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\Data\master.mdf;Integrated Security=True";
        private  SqlConnection SQLConnection;
        private Query query;

        public DatabaseTier()
        {
            SQLConnection = new SqlConnection(connectionString);
            //ResetDatabase();
            DBHelper.SetWorkingDatabase(this);
            DBHelper.DataNames = GetDataNames();
        }
        public bool SubmitQuery(Query query)
        {
            this.query = query;
            return true;
        }
        public void RunSQL(string cmd)
        {
            try
            {
                SQLConnection.Open();
                SqlCommand command = new SqlCommand(cmd, SQLConnection);
                command.CommandTimeout = 1000;
                command.ExecuteNonQuery();
                command.CommandTimeout = 1000;
                SQLConnection.Close();
            }
            catch (Exception)
            {
                SQLConnection.Close();
                throw new Exception("Invalid Syntax.");
            }

        }
        public object[] RunQuery()
        {
            if (this.query == null)
                return null;
            else
            {
                try
                {
                    object[] results = null;
                    string commands = query.GetSQLCommand();
                    SQLConnection.Open();
                    if (!(query is SelectQuery))
                    {
                        SqlCommand command = new SqlCommand(commands, SQLConnection);
                        command.ExecuteNonQuery();
                    }
                    SqlCommand selectResults = new SqlCommand(query.ToSelectSQL(), SQLConnection);
                    selectResults.CommandTimeout = 1000;

                    using (SqlDataReader Reader = selectResults.ExecuteReader())
                    {
                        string[] fieldnames = new string[Reader.FieldCount];
                        for (int i = 0; i < Reader.FieldCount; i++)
                        {
                            fieldnames[i] = Reader.GetName(i);
                        }
                        List<string[]> process = new List<string[]>();
                        while (Reader.Read())
                        {
                            string[] tmp = new string[Reader.FieldCount];
                            for (int i = 0; i < Reader.FieldCount; i++)
                            {
                                tmp[i] = Reader.GetValue(i).ToString();
                            }
                            process.Add(tmp);
                        }
                        results = EntityHelper.GetObjectsFromFields(query.DataName, fieldnames, process);
                        query.SetResults(results);
                        Reader.Close();
                    }
                    return results;
                }
                catch
                {
                    return null;
                }
                finally
                {
                    SQLConnection.Close();
                }
            }
            
        }

        public void Flush()
        {
            this.query = null;
        }
        public bool RequestStop()
        {
            SQLConnection.Dispose();
            return true;
        }
        public Type GetTypeFromDBName(string dataName)
        {
            try
            {
                return EntityHelper.GetTypeFromShortString(dataName);
            }
            catch
            {
                throw new NullReferenceException("List is empty (DatabaseTier.GetTypeFromDBName)");
            }
        }

        private bool AddData(string dataName, DynamicEntity obj)
        {
            return false;
        }
        private string[] GetDataNames()
        {
            return new string[] { "BoughtProduct", "ClubMember", "Customer", "Department", "Employee", "Product", "Transaction" };
        }

        bool IDatabaseTier.DeleteObjects(string dataName, object[] obj)
        {
            DeleteQuery dq = new DeleteQuery();
            dq.SetDataName(dataName);
            Type t = EntityHelper.GetTypeFromShortString(dataName);
            PropertyInfo[] prop = new PropertyInfo[] { EntityHelper.GetPropertyByName(EntityHelper.GetTypeFromShortString(dataName), "ID") };
            for (int i = 0; i < obj.Length; i++)
            {
                DynamicEntity tmp = (DynamicEntity)obj[i];
                dq.SetWhere(new SingleCondition("ID = "+tmp.ID,prop));
                SubmitQuery(dq);
                RunQuery();
            }

            return true;
        }
        bool IDatabaseTier.Save(string dataName, object[] obj)
        {
            string error_log="";
            InsertQuery iq = new InsertQuery();
            iq.SetDataName(dataName);
            EditQuery eq = new EditQuery();
            eq.SetDataName(dataName);
            for (int i = 0; i < obj.Length; i++)
            {
                if(((DynamicEntity)obj[i]).ID!=-1)
                {
                    try
                    {
                        RunSQL(eq.GetEditSQL((DynamicEntity)obj[i], dataName));
                    }
                    catch
                    {
                        error_log += i + ", ";
                    }
                }
                else
                {
                    iq.SetInsert((DynamicEntity)obj[i]);
                    SubmitQuery(iq);
                    RunQuery();
                }
            }
            return true;
        }
        // testing
        private void resetDatabase()
        {

        }
    }
}