﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.DynamicQuerying.Attributes;
using Database.DynamicQuerying.QueryWrappers.NonImplementation;

namespace Database.DynamicQuerying.QueryWrappers.Helpers
{
    public class QueryWrapperHelper
    {
        public static MethodInfo[] GetSteps(Type qt)
        {
            MethodInfo[] mi = qt.GetMethods(BindingFlags.Public | BindingFlags.Instance);
            int count = mi.Length;
            for (int i = 0; i < mi.Length; i++)
                if (mi[i].GetCustomAttribute(typeof(StepAttribute)) == null)
                {
                    mi[i] = null;
                    count--;
                }
            return packAndFlip(mi, count);
        }
        private static MethodInfo[] packAndFlip(MethodInfo[] arr, int count)
        {
            MethodInfo[] ans = new MethodInfo[count];
            int j = count - 1;
            try
            {
                for (int i = 0; i < arr.Length; i++)
                    if (arr[i] != null)
                    {
                        ans[j] = arr[i];
                        j--;
                    }
            }
            catch
            {
                throw new ArgumentException("wrong count argument (QueryHelper.pack)");
            }
            return ans;
        }
        public static string PrintArray(string[] arr, bool addLineNumber)
        {
            string ans = "";
            for (int i = 0; i < arr.Length; i++)
            {
                if (addLineNumber)
                    ans += i + ". ";
                ans += arr[i] + "\n";
            }
            return ans;
        }
    }
}