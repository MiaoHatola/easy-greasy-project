﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Beckend.Entity;
using Beckend.Entity.BasicEntities;
using System.Reflection;
using Beckend.DynamicQuerying;
using Beckend.DynamicQuerying.Conditions;
using Beckend.DynamicQuerying.Attributes;
using Database.DynamicQuerying.QueryWrappers.Helpers;

namespace Database.DynamicQuerying.QueryWrappers.NonImplementation
{
    public class WhereQWrapper : QWrapper
    {
        WhereQuery wq;

        public WhereQWrapper(WhereQuery wq) 
            : base(wq) 
        {
            this.wq = wq;
        }
        protected override void UpdateProperties()
        {
            if (!stringHelpers.ContainsKey("SetWhereWrapper") && workingType != null)
            {
                string menu, help, example;

                menu = "Type in your search conditions:\n";
                menu += EntityHelper.PrintProperties(EntityHelper.GetReadableProperties(workingType), true);
                help = "You should type in the number \nrepresnting your choice from the menu above, \nand then hit Enter.\nYou can type \"all\" to choose all,\nor \"none\" to choose nothing.\n";

                example = "1 > 1990 & 1 < 2015 | 2 = 15/06/96";

                addMenus("SetWhereWrapper", menu, help, example);
            }
            if (!options.ContainsKey("SetWhereWrapper") && workingType != null)
            {
                PropertyInfo[] pi = EntityHelper.GetReadableProperties(workingType);
                string[] arr = new string[pi.Length];
                for (int i = 0; i < arr.Length; i++)
                    arr[i] = pi[i].Name;
                options.Add("SetWhereWrapper", arr);
            }
            base.UpdateProperties();
        }
        public override Type GetEntityType()
        {
            return typeof(WhereQWrapper);
        }
        [Step("SetWhere", StepAttribute.StepOptionType.FreeText, 1, 1)]
        public int SetWhereWrapper(string cmd)
        {
            if (cmd.ToUpper().Equals("ALL"))
                wq.SetWhere(new AllCondition());
            else if (cmd.ToUpper().Equals("NONE"))
                wq.SetWhere(new NoneCondition());
            else try
            {
                wq.SetWhere(new OrCondition(cmd, EntityHelper.GetReadableProperties(workingType)));
            }
            catch
            {
                return -2;
            }
            return -1;
        }
    }
}
