﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.DynamicQuerying;
using Beckend.DynamicQuerying.Attributes;

namespace Database.DynamicQuerying.QueryWrappers.NonImplementation
{
    public interface IQueryWrapper 
    {
        Query GetQuery();
        Type GetQueryType();
        Type GetEntityType();
        Type GetWorkingType();
        MethodInfo[] GetSteps();
        object GetRelevantObject(int methodIndex);
        // Helpers
        string GetMenu(string methodName);
        string GetHelp(string methodName);
        string GetExample(string methodName);
        object[] GetOptions(string methodName);
        void ForgetStepHelpers(string methodName);
    }
}
