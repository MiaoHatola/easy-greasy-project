﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend;
using Beckend.Entity;
using Beckend.DynamicQuerying;
using Database.DynamicQuerying.QueryWrappers.Helpers;
using Beckend.DynamicQuerying.Attributes;

namespace Database.DynamicQuerying.QueryWrappers.NonImplementation
{
    public class QWrapper : DynamicEntity, IQueryWrapper
    {
        public static string ExampleNotFound = "No example was found.";
        public static string HelpNotFound = "No help was found.";
        public static string MenuNotFound = "No menu was found.";

        protected Query q;
        protected Type workingType;
        protected MethodInfo[] steps;
        /*
         * dictionary stringHelpers:
         * 
         * < MethodName , (Helpers) >
         *                   menu
         *                   help
         *                   example
         */
        protected Dictionary<string, string[]> stringHelpers;
        protected Dictionary<string, object[]> options;
        protected Dictionary<string, DynamicEntity[]> existingOptions;

        public QWrapper(Query q)
        {
            this.q = q;
            stringHelpers = new Dictionary<string, string[]>();
            options = new Dictionary<string, object[]>();
            existingOptions = new Dictionary<string, DynamicEntity[]>();
            steps = QueryWrapperHelper.GetSteps(GetEntityType());
            workingType = null;
            UpdateProperties();
        }
        public Query GetQuery()
        {
            return this.q;
        }
        public Type GetQueryType()
        {
            return q.GetEntityType();
        }
        public override Type GetEntityType()
        {
            return typeof(QWrapper);
        }
        Type IQueryWrapper.GetWorkingType()
        {
            return this.workingType;
        }
        MethodInfo[] IQueryWrapper.GetSteps()
        {
            return steps;
        }
        public virtual object GetRelevantObject(int n)
        {
            return this;
        }
        public string GetMenu(string methodName)
        {
            if (stringHelpers.ContainsKey(methodName))
                return stringHelpers[methodName][0];
            else return MenuNotFound;
        }
        public string GetHelp(string methodName)
        {
            if (stringHelpers.ContainsKey(methodName))
                return stringHelpers[methodName][1];
            else return HelpNotFound;
        }
        public string GetExample(string methodName)
        {
            if (stringHelpers.ContainsKey(methodName))
                return stringHelpers[methodName][2];
            else return ExampleNotFound;
        }
        public object[] GetOptions(string methodName)
        {
            if (options.ContainsKey(methodName))
                return options[methodName];
            else return null;
        }
        public DynamicEntity[] GetExistingOptions(string methodName)
        {
            if (existingOptions.ContainsKey(methodName))
                return existingOptions[methodName];
            else return null;
        }
        protected void addMenus(string methodName, string menu, string help, string example)
        {
            stringHelpers.Add(methodName, new string[] { menu, help, example });
        }
        private string generateExample
        {
            get
            {
                int l = Database.DBHelper.DataNames.Length;
                Random rnd = new Random();
                return Database.DBHelper.DataNames[rnd.Next(0, l)];
            }
        }
        public void ForgetStepHelpers(string methodName)
        {
            options.Remove(methodName);
            stringHelpers.Remove(methodName);
        }
        protected virtual void UpdateProperties()
        {
            if (!stringHelpers.ContainsKey("SetDataName"))
            {
                // add helpers relevant to this class
                string menu = "Please choose a data type for your query:\n";
                string help = "You should type in the name or index \nof the entity you wish to choose \nfrom the menu above, and then hit Enter.\n";

                addMenus("SetDataName", menu, help, generateExample);
            }
            if (!options.ContainsKey("SetDataName"))
            {
                options.Add("SetDataName", DBHelper.DataNames);
            }
        }
        
        /* Steps */
        [Step("SetData", StepAttribute.StepOptionType.SingleOption, 1, 1)]
        public int SetDataName(int dataNameIndex)
        {
            string dataName = DBHelper.DataNames[dataNameIndex];

            q.SetDataName(dataName);
            try
            {
                workingType = DBHelper.GetWorkingDatabase().GetTypeFromDBName(dataName);

            }
            catch
            {
                return 0;
            }
            UpdateProperties();
            return -1;
        }
    }
}