﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Beckend.DynamicQuerying.Queries;
using Database.DynamicQuerying.QueryWrappers.NonImplementation;
using Beckend.DynamicQuerying.Attributes;

namespace Database.DynamicQuerying.QueryWrappers
{
    public class DeleteQWrapper : WhereQWrapper
    {
        DeleteQuery dq;

        public DeleteQWrapper()
            : base(new DeleteQuery())
        {
            this.dq = (DeleteQuery)GetQuery();
        }
        public override Type GetEntityType()
        {
            return typeof(DeleteQWrapper);
        }
    }
}
