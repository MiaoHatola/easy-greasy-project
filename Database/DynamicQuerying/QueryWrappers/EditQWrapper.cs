﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.DynamicQuerying.Queries;
using Beckend.Entity;
using Database.DynamicQuerying.QueryWrappers.NonImplementation;
using Beckend.DynamicQuerying.Attributes;

namespace Database.DynamicQuerying.QueryWrappers
{
    public class EditQWrapper : WhereQWrapper
    {
        EditQuery eq;

        public EditQWrapper()
            : base(new EditQuery())
        {
            this.eq = (EditQuery)GetQuery();
        }
        protected override void UpdateProperties()
        {
            if (!stringHelpers.ContainsKey("SetEditWrapper") && workingType != null)
            {
                string menu, help, example;

                menu = "What do you wish to edit?\n";
                menu += EntityHelper.PrintProperties(EntityHelper.GetWritableProperties(workingType), true);
                help = "You need to type the numbers \n represnting the properties you would like to change \n and the values to set them to in the right after, \n with blank in between them, \n and then hit Enter.\n";

                example = "1 George 2 01/01/2015";

                addMenus("SetEditWrapper", menu, help, example);
            }
            if (!options.ContainsKey("SetEditWrapper") && workingType != null)
            {
                PropertyInfo[] pi = EntityHelper.GetWritableProperties(workingType);
                string[] arr = new string[pi.Length];

                for (int i = 0; i < arr.Length; i++)
                    arr[i] = pi[i].Name;
                options.Add("SetEditWrapper", arr);
            }
            base.UpdateProperties();
        }
        public override Type GetEntityType()
        {
            return typeof(EditQWrapper);
        }
        [Step("SetEdit", StepAttribute.StepOptionType.FreeText, 1, 1)]
        public int SetEditWrapper(string cmd)
        {
            string[] input = cmd.Split(' ');
            PropertyInfo[] pi = EntityHelper.GetWritableProperties(workingType);

            PropertyInfo[] toSet = new PropertyInfo[input.Length / 2];
            string[] args = new string[input.Length / 2];

            for (int i = 0; i < input.Length / 2; i++)
            {
                try
                {
                    toSet[i] = pi[int.Parse(input[2 * i])];
                    args[i] = input[2 * i + 1];
                }
                catch
                {
                    return i;
                }
            }
            eq.SetEdit(toSet, args);
            UpdateProperties();
            return -1;
        }
    }
}
