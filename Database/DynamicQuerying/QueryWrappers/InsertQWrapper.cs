﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Database.DynamicQuerying.QueryWrappers.NonImplementation;
using Beckend.DynamicQuerying.Attributes;
using Database.DynamicQuerying.QueryWrappers.Helpers;
using Beckend.DynamicQuerying.Queries;
using Beckend.Entity;

namespace Database.DynamicQuerying.QueryWrappers
{
    public class InsertQWrapper : QWrapper
    {
        DynamicEntity newObject;
        InsertQuery iq;
        bool stepsUpdated;

        public InsertQWrapper()
            : base(new InsertQuery()) 
        {
            this.iq = (InsertQuery)GetQuery();
            stepsUpdated = false;
        }
        protected override void UpdateProperties()
        {
            if (!stringHelpers.ContainsKey("SetInsertWrapper") && workingType != null)
            {
                string menu, help, example;

                menu = "Please enter suitable parameters:\n";
                menu += EntityHelper.PrintParameters(EntityHelper.GetConstructorParameters(workingType), true);

                help = "Type in the parameters for building a proper " + workingType.ToString().Split('.').Last() + "\n (consider the suitable types printed \n aside of every parameter name).\n";
                example = "Jesus Christ 01/01/0001";

                if (!options.ContainsKey("SetInsertWrapper") && workingType != null)
                {
                    ParameterInfo[] pi = EntityHelper.GetConstructorParameters(workingType);
                    string[] arr = new string[pi.Length];

                    for (int i = 0; i < arr.Length; i++)
                        arr[i] = pi[i].Name + "\t" + pi[i].ParameterType;
                    options.Add("SetInsertWrapper", arr);
                }

                addMenus("SetInsertWrapper", menu, help, example);
            }

            if (!stepsUpdated && workingType != null)
            {
                MethodInfo[] mi1 = QueryWrapperHelper.GetSteps(GetEntityType());
                MethodInfo[] mi2 = QueryWrapperHelper.GetSteps(workingType);

                steps = new MethodInfo[mi1.Length + mi2.Length];
                mi1.CopyTo(steps, 0);
                mi2.CopyTo(steps, mi1.Length);

                stepsUpdated = true;
            }
            if (!options.ContainsKey("SetInsertWrapper") && workingType != null)
            {
                ParameterInfo[] pi = EntityHelper.GetConstructorParameters(workingType);
                string[] arr = new string[pi.Length];
                for (int i = 0; i < arr.Length; i++)
                {
                    arr[i] = pi[i].Name + " (" + pi[i].ParameterType + ")";
                }
                options.Add("SetInsertWrapper", arr);
            }
            base.UpdateProperties();
        }
        public override Type GetEntityType()
        {
            return typeof(InsertQWrapper);
        }
        public override object GetRelevantObject(int n)
        {
            if (steps[n].DeclaringType == workingType)
                return newObject;
            else return this;
        }
        [Step("SetInsert", StepAttribute.StepOptionType.FreeText, 1, 1)]
        public int SetInsertWrapper(string cmd)
        {
            object[] o = cmd.Split(' ');
            return SetInsertWrapper(o);
        }
        public int SetInsertWrapper(object[] parameters)
        {
            ParameterInfo[] pi = EntityHelper.GetConstructorParameters(workingType);
            int i = -1;
            try
            {
                for (i = 0; i < pi.Length; i++)
                {
                    parameters[i] = Convert.ChangeType(parameters[i], pi[i].ParameterType);
                }
                iq.SetInsert(newObject = (DynamicEntity)Activator.CreateInstance(workingType, parameters));
            }
            catch
            {
                return i;
            }
            UpdateProperties();
            return -1;
        }
    }
}