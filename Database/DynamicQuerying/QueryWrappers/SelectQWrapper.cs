﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Beckend.Entity;
using Beckend.DynamicQuerying;
using Beckend.DynamicQuerying.Queries;
using Database.DynamicQuerying.QueryWrappers.NonImplementation;
using Beckend.DynamicQuerying.Attributes;

namespace Database.DynamicQuerying.QueryWrappers
{
    public class SelectQWrapper : WhereQWrapper
    {
        SelectQuery sq;

        public SelectQWrapper()
            : base(new SelectQuery())
        {
            sq = (SelectQuery)GetQuery();
        }
        public override Type GetEntityType()
        {
            return typeof(SelectQWrapper);
        }
        protected override void UpdateProperties()
        {
            /*
            if (!stringHelpers.ContainsKey("SetSelectWrapper") && workingType != null)
            {
                string menu, help, example;

                menu = "What should the query print?\n";
                menu += EntityHelper.PrintProperties(EntityHelper.GetReadableProperties(workingType), true);
                help = "You should type in the numbers represnting your choices from the menu above,\nwith blank spaces in between them,and then hit Enter.\n";

                example = "1 2 4 (order your choices the same way you want the program to print them)";

                addMenus("SetSelectWrapper", menu, help, example);
            }
            if (!options.ContainsKey("SetSelectWrapper") && workingType != null)
            {
                PropertyInfo[] pi = EntityHelper.GetReadableProperties(workingType);
                string[] arr = new string[pi.Length];
                for (int i = 0; i < arr.Length; i++)
                    arr[i] = pi[i].Name;
                options.Add("SetSelectWrapper", arr);
            }
             */
            base.UpdateProperties();
        }
        /* Steps */
        /*
        [Step("SetSelect", 1, 1)]
        public int SetSelectWrapper(string propertys)
        {
            string[] tmp = propertys.Split(' ');
            PropertyInfo[] pi = EntityHelper.GetReadableProperties(workingType);
            PropertyInfo[] ans = new PropertyInfo[tmp.Length];
            for (int i = 0; i < ans.Length; i++)
            {
                try
                {
                    ans[i] = pi[int.Parse(tmp[i])];
                }
                catch
                {
                    return i;
                }
            }
            sq.SetSelect(ans);
            UpdateProperties();
            return -1;
        }
         */
    }
}
