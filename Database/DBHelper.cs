﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database
{
    public class DBHelper
    {
        private static DatabaseTier dbt;
        public static string[] DataNames;
        public static void SetWorkingDatabase(DatabaseTier dbt)
        {
            DBHelper.dbt = dbt;
        }
        public static DatabaseTier GetWorkingDatabase()
        {
            return DBHelper.dbt;
        }
    }
}
