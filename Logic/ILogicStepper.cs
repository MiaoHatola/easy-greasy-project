﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Beckend.Entity.BasicEntities;
using Beckend.DynamicQuerying.Attributes;
using Beckend.DynamicQuerying;
using Beckend.Entity;

namespace Logic
{
    public interface ILogicStepper
    {
        bool RequestStop();
        bool Save(string dataName, params object[] obj);
        bool IsFinished();
        void Build(object parapeters);
        object[] RunQuery(Query q);
        object[] GetResults();
        void RunSQL(string msg);
        string GetExample();
        string GetHelp();
        string GetMenu();
        object[] GetOptions();
        void StartSteps(int n);
        StepAttribute.StepOptionType GetStepType();
        bool Back();
        void Reset();
        User LoginSequence(string username, string password);
        object[] GetAllData(string dataName);
        bool DeleteObjects(string dataName, params object[] obj);
        Query GetBuiltQuery();
    }
}
