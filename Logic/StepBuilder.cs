﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Database.DynamicQuerying.QueryWrappers.NonImplementation;
using Database.DynamicQuerying.QueryWrappers.Helpers;
using Beckend.DynamicQuerying;
using Beckend.DynamicQuerying.Attributes;

namespace Logic
{
    /// <summary>
    /// This class is the builder class, capable of creating queries step-by-step.
    /// This class allows for a VERY dynamic query-building process.
    /// </summary>
    public class StepBuilder
    {
        private IQueryWrapper qw;
        private MethodInfo[] steps { get { return qw.GetSteps(); } }
        private int stepOcurance;
        private int position;

        //initiate a step builder with no specific query wrapper.
        public StepBuilder()
        {
            position = 0;
            stepOcurance = 0;
        }

        //initiate a step builder with a specified query wrapper.
        public StepBuilder(IQueryWrapper qw)
        {
            position = 0;
            this.qw = qw;
            stepOcurance = 0;
        }

        //Builds the queru and sends it to the database for execution.
        public int Build(object parameters)
        {
            int d;

            if (stepOcurance >= getMin() && getCurrentStepAttribute().IsOptional && parameters.Equals(getCurrentStepAttribute().StopSign))
            {
                stepForward();
                d = -1;
            }
            else
            {
                d = (int)steps[position].Invoke(qw.GetRelevantObject(position), new object[] { parameters });
                if (d == -1)
                {
                    raiseOcurance();
                    if (stepOcurance >= getMin() && !getCurrentStepAttribute().IsOptional)
                        stepForward();
                }
            }
            return d;
        }

        //checks if the building process is finished.
        public bool IsFinished()
        {
            return position >= steps.Length;
        }

        //returns the finished query.
        public Query GetBuiltQuery()
        {
            if (IsFinished())
                return qw.GetQuery();
            return null;
        }
        
        //gets the current attribute for dynamically quering different object types.
        private StepAttribute getCurrentStepAttribute()
        {
            return (StepAttribute)steps[position].GetCustomAttribute(typeof(StepAttribute));
        }

        //Gets the available menu (options).
        public string GetMenu()
        {
            if (isOptional())
                return (getCurrentStepAttribute()).OptionQuestion;
            else
                return qw.GetMenu(steps[position].Name);
        }

        //gets help for the relevant menu.
        public string GetHelp()
        {
            if (isOptional())
                return "Enter " + (getCurrentStepAttribute()).StopSign + " when finished";
            else
                return qw.GetHelp(steps[position].Name);
        }

        //gets an example of how to use the current step.
        public string GetExample()
        {
            return "Example: " + qw.GetExample(steps[position].Name);
        }

        //gets the available options for the current step.
        public object[] GetOptions()
        {
            return qw.GetOptions(steps[position].Name);
        }

        //gets the step type (steps have multiple types: single, multiple and free text)
        public StepAttribute.StepOptionType GetStepType()
        {
            return getCurrentStepAttribute().StepType;
        }

        private void raiseOcurance()
        {
            stepOcurance++;
        }
        private int getMin()
        {
            return (getCurrentStepAttribute()).Min;
        }
        private int getMax()
        {
            return (getCurrentStepAttribute()).Max;
        }
        private bool isOptional()
        {
            return (getCurrentStepAttribute()).IsOptional;
        }
        private string getStopSign()
        {
            if (isOptional())
                return (getCurrentStepAttribute()).StopSign;
            else
                throw new FieldAccessException(steps[position].Name + " is not optional and doesn't have a stop sign.");
        }
        private string getOptionQuestion()
        {
            if (isOptional())
                return (getCurrentStepAttribute()).OptionQuestion;
            else
                throw new FieldAccessException(steps[position].Name + " is not optional and doesn't have an option question sign.");
        }
        protected void stepForward()
        {
            stepOcurance = 0;
            position++;
        }
        public bool stepBack()
        {
            if (position > 0)
            {
                qw.ForgetStepHelpers(steps[position].Name);
                stepOcurance = 0;
                position--;
                return true;
            }
            return false;

        }
        public void resetSteps()
        {
            stepOcurance = 0;
            position = 0;
        }
    }
}