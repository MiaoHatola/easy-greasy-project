﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Beckend.DynamicQuerying.Attributes;
using Database;
using Beckend.DynamicQuerying.Queries;
using Beckend.Properties;
using Database.DynamicQuerying.QueryWrappers;
using Database.DynamicQuerying.QueryWrappers.NonImplementation;
using System.Reflection;
using Beckend.Entity;
using Beckend.DynamicQuerying.Conditions;
using Beckend.Entity.BasicEntities;
using Beckend.DynamicQuerying;

namespace Logic
{
    public class LogicTier : ILogicStepper
    {
        public static object[] options = new object[] { "Select", "Insert", "Edit", "Delete" };
        private static ILogicStepper currentInstance;
        public static ILogicStepper GetInstance()
        {
            if (currentInstance == null)
                currentInstance = new LogicTier();
            return currentInstance;
        }
        
        IDatabaseTier dbt;
        private StepBuilder sb = new StepBuilder();

        private LogicTier()
        {
            dbt = new DatabaseTier();
        }

        bool ILogicStepper.RequestStop()
        {
            return dbt.RequestStop();
        }
        bool ILogicStepper.Save(string dataName, params object[] obj)
        {
            return dbt.Save(dataName, obj);
        }
        object[] ILogicStepper.RunQuery(Query q)
        {
            if (!dbt.SubmitQuery(q))
                return null;
            else return dbt.RunQuery();
        }
        void ILogicStepper.StartSteps(int n)
        {
            QWrapper q;
            switch (n)
            {
                case 0: q = new SelectQWrapper();
                    break;
                case 1: q = new InsertQWrapper();
                    break;
                case 2: q = new EditQWrapper();
                    break;
                case 3: q = new DeleteQWrapper();
                    break;
                default: throw new ArgumentException("No such option exists (LogicTier.Constructor)");
            }
            sb = new StepBuilder(q);
        }
        bool ILogicStepper.Back()
        {
            if (sb.stepBack())
                return true;
            return false;
        }
        void ILogicStepper.Build(object obj)
        {
            sb.Build(obj);
            if (sb.IsFinished() && dbt.SubmitQuery(sb.GetBuiltQuery()))
                dbt.RunQuery();
        }
        public void Reset()
        {
            sb.resetSteps();
        }
        public void RunSQL(string msg)
        {
            try
            {
                dbt.RunSQL(msg);
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public bool IsFinished()
        {
            return sb.IsFinished();
        }
        public object[] GetResults()
        {
            return sb.GetBuiltQuery().GetResults();
        }
        public StepAttribute.StepOptionType GetStepType()
        {
            return sb.GetStepType();
        }
        public object[] GetOptions()
        {
            return sb.GetOptions();
        }
        string ILogicStepper.GetMenu()
        {
            return sb.GetMenu();
        }
        string ILogicStepper.GetHelp()
        {
            return sb.GetHelp();
        }
        string ILogicStepper.GetExample()
        {
            return sb.GetExample();
        }
        User ILogicStepper.LoginSequence(string username, string password)
        {
            string[] datanames = new string[]{ "Employee", "ClubMember" , "Customer" };
            object user = null;
            int i = 0;
            SelectQuery checkUser = new SelectQuery();
            while (user == null) 
            { 
            //set Properties to check for login sequence
                if (i == datanames.Length)
                    return null;
                
                    Type typ = EntityHelper.GetTypeFromShortString(datanames[i]);
                    PropertyInfo[] byPropertys = { EntityHelper.GetPropertyByName(typ, "Username"), EntityHelper.GetPropertyByName(typ, "Password") };
                    string[] byArgs = { username, password };
                    int[] byComparers = { 0, 0 };

                    checkUser.SetWhere(new AndCondition(byPropertys, byComparers, byArgs));
                    checkUser.SetDataName(datanames[i]);

                    dbt.SubmitQuery(checkUser);

                    object[] results = dbt.RunQuery();
                    if (results == null || results.Length == 0)
                        i++;
                    else
                    {
                        return (User)results[0];
                    }
                
            }
            return null;
        }
        object[] ILogicStepper.GetAllData(string dataName)
        {
            SelectQuery sq = new SelectQuery();

            //set Properties to get all Customers
            sq.SetWhere(new AllCondition());
            sq.SetDataName(dataName);
            if (dbt.SubmitQuery(sq))
                return dbt.RunQuery();
            else
                return null;
        }
        bool ILogicStepper.DeleteObjects(string dataName, params object[] obj)
        {
            return dbt.DeleteObjects(dataName, obj);
        }
        Query ILogicStepper.GetBuiltQuery()
        {
            return sb.GetBuiltQuery();
        }
    }
}